add_executable( VideoTest VideoTest.cpp )
target_link_libraries( VideoTest  ${IPChain_LIBRARIES})

add_executable( ImageTest ImageTest.cpp )
target_link_libraries( ImageTest ${IPChain_LIBRARIES})

add_executable( ContoursTest ContoursTest.cpp )
target_link_libraries( ContoursTest opencv_core opencv_highgui opencv_imgproc)

add_executable( RegistryTest RegistryTest.cpp )
target_link_libraries( RegistryTest  ${IPChain_LIBRARIES})

add_executable( RodbotTest RodbotTest.cpp )
target_link_libraries( RodbotTest  ${IPChain_LIBRARIES})

add_executable(Utils Utils.cpp)
target_link_libraries(Utils ${IPChain_LIBRARIES})
