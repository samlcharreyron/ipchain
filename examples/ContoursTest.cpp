/*
 * ContoursTest.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: Sam
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;
int main( int argc, char** argv )
{
	typedef std::vector<cv::Point> Contour;
    typedef std::vector<cv::Vec4i> ContourHierarchy;
    //typedef boost::shared_ptr<Contour> Contour_p;
    
    std::string image_dir = argv[1];
	cv::Mat image = imread(image_dir, CV_LOAD_IMAGE_GRAYSCALE);

	if( argc != 2 || ! image.data)
	{
		cout << "Could not open image" << endl;
		return -1;
	}

    Mat lines;
    HoughLines(image,lines,1,CV_PI/180,100,0,0);

	//ContourHierarchy hierarchy;
	//findContours( image, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
    
    
    //cout << lines.col(0) << endl;

}
