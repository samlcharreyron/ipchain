/*
 * ImageTest.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: Sam
 */

#include <iostream>
#include <iterator>
#include <string>
#include <memory>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/lexical_cast.hpp>

#include <IPChain/IPChain.hpp>
#include <IPChain/IPUnits/IPUnits.hpp>

using namespace cv;
using namespace std;
using namespace IRIS::Vision;


IPChain chain;

BlurUnit * p_blur_u;
BlurUnit * p_median_u;
CannyUnit * p_canny_u;
AdaptiveThresholdUnit * p_adapt_u;
ContoursUnit * p_contours_u;
MorphologyUnit * p_dilate_u;

int blur_size 		= 5;
int median_size 	= 5;
int block_size 		= 25;
int eps_sf_i 		= 2;
int dilate_size 	= 3;

Mat frame, frame_gray, out;

void applyChain(int, void*);

int main( int argc, char** argv )
{
    std::string image_dir = argv[1];
	cv::Mat image = imread(image_dir, CV_LOAD_IMAGE_GRAYSCALE);

	if( argc != 2 || ! image.data)
	{
		cout << "Could not open image" << endl;
		return -1;
	}

	//waitKey(0);
	UnitSettings blur_settings;
    blur_settings["size"] = boost::lexical_cast<std::string>(blur_size);
    blur_settings["type"] = boost::lexical_cast<std::string>(SmoothingTypes::BOX);

	UnitSettings median_settings;
    median_settings["size"] = boost::lexical_cast<std::string>(median_size);
    median_settings["type"] = boost::lexical_cast<std::string>(SmoothingTypes::MEDIAN);

	p_blur_u 		= new BlurUnit(blur_settings);
	p_adapt_u 		= new AdaptiveThresholdUnit;
	p_median_u 		= new BlurUnit(median_settings);

	// Contours unit
	//ContoursSettings contours_options;
	//contours_options.eps_area_sf = eps_sf_i/1000.0;
	//p_contours_u 	= new ContoursUnit(contours_options);

	IPUnit * p_harris = new HarrisCornerUnit;
	UnitSettings settings = p_harris->getSettings();
	settings["normalize"] = "0";
	settings["thresh"] 	  = "100";
	p_harris->applySettings(settings);

	//chain.Append(p_blur_u);
	//chain.Append(p_median_u);
	//chain.Append(p_adapt_u);
	//chain.Append(new CannyUnit);
    //chain.Append(new HoughLinesUnit);
	//chain.Append(new HoughCirclesUnit);
	chain.Append(new BlobDetectorUnit);
	//chain.Append(p_harris);

	for(IPChain::const_iterator it = chain.begin(); it != chain.end(); it++) {
		std::cout << **it << endl;
	}

	//chain.Move(1,p_median_u);

	namedWindow( "processed", CV_WINDOW_AUTOSIZE );
	createTrackbar( "blur radius",
			"processed", &blur_size,
			16, applyChain );

	createTrackbar( "median radius",
			"processed", &median_size,
			16, applyChain );

	createTrackbar( "block size",
			"processed", &block_size,
			50, applyChain );

	createTrackbar( "dilation size",
			"processed", &dilate_size,
			11, applyChain);

	createTrackbar( "eps factor",
			"processed", &eps_sf_i,
			10, applyChain );

	for (;;) {
		chain.ProcessImage(image,out);
		imshow( "processed", out );

		char c = waitKey(33);
		if (c == 27) break;
		if (c != -1) {
			try {
				int unit_num = boost::lexical_cast<short>(c);

				if (unit_num < 1 || unit_num > chain.size()) {
					cout << "Goodbye" << endl;
					return 0;
				}

				bool enabled = chain[unit_num].isActive();
				chain[unit_num-1].toggle(!enabled);

			} catch (boost::bad_lexical_cast& e) {
				cout << "Goodbye!" << endl;
				return 0;
			}
		}
	}

	return 0;
}

void applyChain(int, void*) {
	UnitSettings smoothing_settings = p_blur_u->getSettings();
    smoothing_settings["size"] = boost::lexical_cast<std::string>(blur_size);
	p_blur_u->applySettings(smoothing_settings);

	UnitSettings median_settings = p_median_u->getSettings();
    median_settings["size"] = boost::lexical_cast<std::string>(blur_size);
	p_median_u->applySettings(median_settings);

	UnitSettings adaptive_settings = p_adapt_u->getSettings();
    adaptive_settings["blockSize"] = boost::lexical_cast<std::string>(block_size);
	p_adapt_u->applySettings(adaptive_settings);
	//p_contours_u->m_options.eps_area_sf = eps_sf_i/1000.0;
}



