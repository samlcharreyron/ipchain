/*
 * RegistryTest.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: Sam
 */

#include <iostream>

#include <IPChain/ChainRegistry.hpp>

template<typename R>
void dumpRegistrar(const R& registrar, const std::string& name)
{
	std::cout << "=== " << name << " ===\n" << std::endl;

	for (typename R::const_iterator it = registrar.begin() ; it != registrar.end(); ++it)
	{
		std::cout << "-" << it->first << std::endl;
        std::cout << it->second->description() << std::endl;
        std::cout << std::endl;
	}
}

using namespace IRIS::Vision;

int main( int argc, char** argv ) {

	ChainRegistry chainReg;
    
	dumpRegistrar(chainReg.IPUnitRegistrar,"IPUnit");
}



