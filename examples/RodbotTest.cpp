/*
 * RodbotTest.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "IPChain/IPChain.hpp"
#include "IPChain/IPUnits/IPUnits.hpp"

using namespace cv;
using namespace std;
using namespace IRIS::Vision;

int main( int argc, char** argv )
{
	Mat frame, frame_gray, out;

	IPChain chain;
	chain.Append(new RodbotBlobDetector);
	chain.Append(new LinesRegularityFilter);
	chain.Append(new RodbotKalmanFilter);

	VideoCapture capture;
	capture.open(argv[1]);

	if( argc != 2 || !capture.isOpened() )
	{
		cout << "Could not open video files" << endl;
		return -1;
	}

	for (;;) {
			capture >> frame;
			cvtColor(frame,frame_gray,CV_RGB2GRAY);
			chain.ProcessImage(frame_gray,out);
			imshow( "processed", out );

			char c = waitKey(33);
			if (c == 27) break;
			if (c != -1) {
				try {
					int unit_num = boost::lexical_cast<short>(c);

					if (unit_num < 1 || unit_num > chain.size()) {
						cout << "Goodbye" << endl;
						return 0;
					}

					bool enabled = chain[unit_num].isActive();
					chain[unit_num].toggle(!enabled);

				} catch (boost::bad_lexical_cast& e) {
					cout << "Goodbye!" << endl;
					return 0;
				}
			}
		}

		return 0;
}

