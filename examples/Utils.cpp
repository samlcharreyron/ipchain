#include "IPChain/Utils/Math.hpp"
#include <opencv2/core/core.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace IRIS::Vision;

int main(int argc, char** argv) {
    string sq_1 = "1,0,0,1";
    string sq_2 = "0,0,1,0,1,0,1,0,0";
    string nsq_1 = "1,0,0,0,1";

    cv::Mat sq_1_m = toSquareCvMat(sq_1);
    cout << sq_1_m << endl;
    assert(sq_1_m.rows == 2 && sq_1_m.cols == 2);

    cv::Mat sq_2_m = toSquareCvMat(sq_2);
    assert(sq_2_m.rows == 3 && sq_2_m.cols == 3);

}
