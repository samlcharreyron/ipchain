/*
 * IPChainVideo.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: Sam
 */
#include <iostream>
#include <iterator>
#include <string>
#include <memory>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/lexical_cast.hpp>

#include <IPChain/IPChain.hpp>
#include <IPChain/IPUnits/IPUnits.hpp>

using namespace cv;
using namespace std;
using namespace IRIS::Vision;


IPChain chain;

BlobDetectorUnit * p_blob_u;
KalmanTrackerKPUnit * p_kalman_u;

int min_area;
int max_area;
int thresh;

Mat frame, frame_gray, out;

void applyChain(int, void*);

int main( int argc, char** argv )
{
	VideoCapture capture;
	capture.open(argv[1]);

    if( argc != 2 || !capture.isOpened() )
	{
		cout << "Could not open video files" << endl;
		return -1;
	}
    
    p_blob_u = new BlobDetectorUnit;
    p_kalman_u = new KalmanTrackerKPUnit;
    p_blob_u->setNextFeaturesUnit(p_kalman_u);
    chain.Append(p_blob_u);
    chain.Append(p_kalman_u);
	
    UnitSettings blob_settings = p_blob_u->getSettings();
    min_area = boost::lexical_cast<int>(blob_settings["minArea"]);
    max_area = boost::lexical_cast<int>(blob_settings["maxArea"]);
    thresh = boost::lexical_cast<int>(blob_settings["minThreshold"]);

	for(IPChain::const_iterator it = chain.begin(); it != chain.end(); it++) {
		std::cout << **it << endl;
	}


	namedWindow( "processed", CV_WINDOW_AUTOSIZE );

    createTrackbar( "min area",
            "processed", &min_area,
            5000, applyChain );
    createTrackbar( "max area",
                   "processed", &max_area,
                   500000, applyChain );
    createTrackbar("thresh",
                   "processed", &thresh,
                   255, applyChain );

	for (;;) {
		capture >> frame;
		//contours_options.background = frame;
		//p_contours_u->SetOptions(contours_options);
		cvtColor(frame,frame_gray,CV_RGB2GRAY);
		chain.ProcessImage(frame_gray,out);
		imshow( "processed", out );

		char c = waitKey(33);
		if (c == 27) break;
		if (c != -1) {
			try {
                int unit_num = boost::lexical_cast<short>(c);

                if (unit_num < 0 || unit_num >= chain.size()) {
					cout << "Goodbye" << endl;
					return 0;
				}

                bool enabled = chain[unit_num].isActive();
                chain[unit_num].toggle(!enabled);

                for(IPChain::const_iterator it = chain.begin(); it != chain.end(); it++) {
                    std::cout << **it << endl;
                }

			} catch (boost::bad_lexical_cast& e) {
				cout << "Goodbye!" << endl;
				return 0;
			}
		}
	}

	return 0;
}

void applyChain(int, void*) {
    UnitSettings blob_settings = p_blob_u->getSettings();
    blob_settings["minArea"] = boost::lexical_cast<std::string>(min_area);
    blob_settings["maxArea"] = boost::lexical_cast<std::string>(max_area);
    blob_settings["minThreshold"] = boost::lexical_cast<std::string>(thresh);
    p_blob_u->applySettings(blob_settings);
//
//	UnitSettings median_settings = p_median_u->getSettings();
//    median_settings["size"] = boost::lexical_cast<std::string>(blur_size);
//	p_median_u->applySettings(median_settings);
//
//	UnitSettings adaptive_settings = p_adapt_u->getSettings();
//    adaptive_settings["blockSize"] = boost::lexical_cast<std::string>(block_size);
//	p_adapt_u->applySettings(adaptive_settings);
    
    for(IPChain::const_iterator it = chain.begin(); it != chain.end(); it++) {
		std::cout << **it << endl;
	}
    
	//p_contours_u->m_options.eps_area_sf = eps_sf_i/1000.0;
}
