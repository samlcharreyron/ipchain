/*
 * ChainRegistry.hpp
 *
 *  Created on: Feb 12, 2014
 *      Author: Sam
 */

#ifndef CHAINREGISTRY_HPP_
#define CHAINREGISTRY_HPP_

#include <IPChain/Registrar.hpp>
#include "IPChain/UnitTypes/ImageFilterUnit.hpp"
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

class ChainRegistry {
public:

	DEF_REGISTRAR(IPUnit)

	static const ChainRegistry& get() {
		static ChainRegistry instance;
		return instance;
	}

	ChainRegistry();
	virtual ~ChainRegistry();

};

} /* namespace Vision */
} /* namespace IRIS */

#endif /* CHAINREGISTRY_HPP_ */
