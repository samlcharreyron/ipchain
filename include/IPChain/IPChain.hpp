#ifndef __IP_Chain_H_
#define __IP_Chain_H_

#include <iostream>
#include <algorithm>
#include <iterator>
#include <list>
#include <opencv2/core/core.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include "IPChain/UnitTypes/UnitTypes.hpp"

//#ifdef HAVE_YAML_CPP
//	#include <yaml-cpp/yaml.h>
//#endif // HAVE_YAML_CPP


namespace IRIS {
namespace Vision {

struct UnitError :    public boost::exception,
        public std::exception {

    UnitError(IPUnit* addr, const std::string& msg) :
        unitAddr(addr),
        what_(addr->name() + " " + msg) {}

    virtual ~UnitError() throw() {}

    virtual const char* what() const throw()
    {
        return what_.c_str();
    }

    IPUnit* unitAddr;
    const std::string what_;
};

struct UnitOpenCVError : public UnitError {
    UnitOpenCVError(IPUnit* addr, const std::string& msg) :
        UnitError(addr, "OpenCV error: " + msg) {}
};

struct UnitInvalidNextFeaturesError : public UnitError {
    UnitInvalidNextFeaturesError(IPUnit* addr, const std::string& msg) :
        UnitError(addr, "Invalid next features unit: ") {}
};

// Wrapper class to hold the IP chain
// Mostly to hold pointer to head of chain, iterators
// and define operations on chain
class IPChain {

    std::list<boost::shared_ptr<IPUnit> > m_unit_list;
    boost::mutex m_mutex;

public:

    ~IPChain();

    // Load a configuration from a YAML file
    void loadFromYaml(const std::string& configFile);

    // Save a configuration to a YAML file
    void saveToYaml(const std::string& configFile);

    typedef std::list<boost::shared_ptr<IPUnit> >::iterator iterator;
    typedef std::list<boost::shared_ptr<IPUnit> >::const_iterator const_iterator;

    void ProcessImage(const cv::Mat& in, cv::Mat& out);

    // Add unit to front of chain
    void Prepend(IPUnit* p_unit);

    // Add unit to back of chain
    void Append(IPUnit* p_unit);

    // Inserts before iterator position
    void Insert(iterator position, IPUnit* p_unit);

    // Insert at position: 0 is first
    void Insert(const int position, IPUnit * p_unit);

    // Removes single p_unit (destructive)
    // also destroys unit
    void Remove(iterator position);

    // ATTENTION: use this to move and watch out doing Remove and then Insert
    // You need to make sure shared_ptr doesnt lose reference counts and delete the unit
    void Move(const int position, IPUnit * p_unit);

    // Remove and destroy unit
    iterator Remove(IPUnit* p_unit);

    // Return position of p_unit in chain or -1 if not in chain
    int Index(IPUnit* p_unit);

    iterator getIterator(IPUnit* p_unit);

    // Iterators
    iterator begin() {
        return m_unit_list.begin();
    }

    iterator end() {
        return m_unit_list.end();
    }

    const_iterator begin() const {
        return m_unit_list.begin();
    }

    const_iterator end() const {
        return m_unit_list.end();
    }


    size_t size() {
        return m_unit_list.size();
    }

    // Get reference of unit at position pos
    // WARNING: does not check bounds, only use for valid positions
    IPUnit& operator[](const int pos);

    // get iterator to IPUnit at position
    // for invalid positions, returns end()
    iterator at(const int pos);

};

std::ostream& operator<<(std::ostream& s, IPChain const& c);

}
}


#endif
