#ifndef CONTOURS_H_
#define CONTOURS_H_

#include <opencv2/imgproc/imgproc.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

typedef boost::shared_ptr<Contour> Contour_p;
typedef std::vector<cv::Vec4i> ContourHierarchy;

struct ContourFeatures {
    Contours contours;
    ContourHierarchy hierarchy;
};

enum ShapeOption {
	TRIANGLES 	= 0x01,
	RECTANGLES 	= 0x02,
	LSHAPE		= 0x04,
	EIGHTSIDES	= 0x08,
	ALL			= TRIANGLES | RECTANGLES | LSHAPE | EIGHTSIDES
};

// A note about ContoursUnit:
// I would prefer to inherit from a less general class than IPUnit, but
// The contours features that are extracted by this unit can't really be held in
// a cv::Mat, The functionality is mostly that of a features unit in that it is split
// between extraction and display. The functions from the features interface are therefore
// implemented with small adaptations
class ContoursUnit : public ExtractFeaturesUnit<Contours> {

public:

	ContoursUnit() { resetDefault(); }

	ContoursUnit(const UnitSettings settings) :
        ExtractFeaturesUnit<Contours>(settings) {
		validateSettings();
	}

//	// Contours output
//	// TODO: Check how memory is managed in cv::vector
//	cv::vector<Contour> getContours() const {
//		return m_contours;
//	}

	static const std::string nameS() {
		return "ContoursUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Extracts polygonal shape contours from the image.";
	}

	const std::string getName() const {
		return "Contours Unit";
	}

	void resetDefault() {
        m_settings["mode"] = boost::lexical_cast<std::string>(static_cast<int>(CV_RETR_EXTERNAL));
        m_settings["method"] = boost::lexical_cast<std::string>(static_cast<int>(CV_CHAIN_APPROX_SIMPLE));
		m_settings["offsetX"] = "0";
		m_settings["offsetY"] = "0";
		m_settings["thickness"] = "1";
		m_settings["lineType"] = "8";
		m_settings["maxLevel"] = "0";
        m_settings["shapeOptions"] = boost::lexical_cast<std::string>(ALL);
		m_settings["areaThresh"] = "120";
		m_settings["epsAreaSf"] = "0.02";
		m_settings["ignoreInside"] = "false";
	}

protected:

	struct ContoursSettings {

		// contour extraction
		int cont_mode, cont_method;
		cv::Point cont_offset;

		// drawing
		int thickness, line_type, max_level;

		// shape extraction
		int shape_options;
		double area_thresh; // threshold for minimum area of shape
		double eps_area_sf; // to set epsilon for polygonal approximation
		   // factor multiplied by area
		bool ignore_inside; // ignore the inner contour

		//cv::Mat background; //if supplied will draw contours over this background

		ContoursSettings() :
	        cont_mode(CV_RETR_EXTERNAL),
	        cont_method(CV_CHAIN_APPROX_SIMPLE),
			cont_offset(cv::Point(0,0)),
			thickness(1),
			line_type(8),
			max_level(0),
			shape_options(ALL),
			area_thresh(120),
			eps_area_sf(0.02),
			ignore_inside(false) {}
	};


	ContoursSettings getSettings() const;

    void extractFeatures(const cv::Mat in, Contours& features);

    void displayFeatures(const Contours& contours,const cv::Mat overlay, cv::Mat& out) const;

	void validateSettings();

private:
   // Contours m_contours;
	ContourHierarchy m_hiearchy;
	cv::Mat m_overlay;

};

class ContoursCentroidUnit : public ProcessFeaturesContoursUnit {
public:
    ContoursCentroidUnit() { resetDefault(); }

    ContoursCentroidUnit(const UnitSettings& settings) :
        ProcessFeaturesContoursUnit(settings) {
        validateSettings();
    }

    static const std::string nameS() {
        return "ContoursCentroidUnit";
    }

    const std::string name() const {
        return nameS();
    }

    static const std::string description() {
        return "Extracts the centroids from contours.";
    }

    const std::string getName() const {
        return "Contours Centroid Unit";
    }

    void resetDefault() {}

protected:

    void validateSettings() {}

    void processFeatures(const Contours &featuresIn, const cv::Mat imageIn);

    void displayFeatures(const Contours &features, const cv::Mat overlay, cv::Mat &out) const;
};

}
}

#endif /* CONTOURS_H_ */
