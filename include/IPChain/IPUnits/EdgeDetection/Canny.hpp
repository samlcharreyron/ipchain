#ifndef CANNY_HPP_
#define CANNY_HPP_

#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

class CannyUnit : public ExtractFeaturesMatUnit {

	void validateSettings();
public:
	CannyUnit() { resetDefault(); }

	CannyUnit(const UnitSettings & settings) :
		ExtractFeaturesMatUnit(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "CannyUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Canny filter for edge detection.";
	}

	const std::string getName() const {
		return "Canny Unit";
	}

	double getThresh1() const;
	double getThresh2() const;
	int getApertureSize() const;
	bool getL2Gradient() const;

	void resetDefault() {
		m_settings["thresh1"] = "100";
		m_settings["thresh2"] = "200";
		m_settings["apertureSize"] = "3";
		m_settings["L2Gradient"] = "false";
	}

protected:
	void extractFeatures(const cv::Mat in, cv::Mat & features);

	void displayFeatures(const cv::Mat & features, const cv::Mat overlay, cv::Mat & out) const;
};

}
}

#endif /* CANNY_HPP_ */
