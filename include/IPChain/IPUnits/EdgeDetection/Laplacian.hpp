/*
 * Laplacian.hpp
 *
 *  Created on: Feb 1, 2014
 *      Author: Sam
 */

#ifndef LAPLACIAN_HPP_
#define LAPLACIAN_HPP_

#include "IPChain/UnitTypes/ImageFilterUnit.hpp"

namespace IRIS {
namespace Vision {
class LaplacianUnit : public ImageFilterUnit {
	void validateSettings();

	struct LaplacianUnitSettings {
		int ksize;
	};

	class LaplacianUnitError : public std::runtime_error {
	public:
		explicit LaplacianUnitError(const std::string& what) : std::runtime_error(what) {}
	};

public:
	LaplacianUnit() { resetDefault() ; }

	LaplacianUnit(const UnitSettings & settings) :
		ImageFilterUnit(settings) {
		validateSettings();
	}

	void filterImage(const cv::Mat in, cv::Mat & out) const;

	static const std::string nameS() {
		return "LaplacianUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Laplace operator which produces directional derivatives of order 2.";
	}

	const std::string getName() const {
		return "Laplacian Unit";
	}

	void resetDefault() {
		m_settings["ksize"]  = "3";
	}

	LaplacianUnitSettings getSettings() const;
};
}
}


#endif /* LAPLACIAN_HPP_ */
