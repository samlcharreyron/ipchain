#ifndef SCHARR_HPP_
#define SCHARR_HPP_

#include "IPChain/UnitTypes/ImageFilterUnit.hpp"

namespace IRIS {
namespace Vision {

class ScharrUnit : public ImageFilterUnit {
	void validateSettings();

	struct ScharrUnitSettings {
		int xorder, yorder;
	};

	class ScharrUnitError : public std::runtime_error {
	public:
		explicit ScharrUnitError(const std::string& what) : std::runtime_error(what) {}
	};

public:
	ScharrUnit() {	resetDefault(); }

	ScharrUnit(const UnitSettings & settings) :
		ImageFilterUnit(settings) {
		validateSettings();
	}

	void filterImage(const cv::Mat in, cv::Mat & out) const;

	static const std::string nameS() {
		return "ScharrUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Scharr operator which is similar to the Sobel operator but provides a more accurate derivative.";
	}

	const std::string getName() const {
		return "Scharr Filter Unit";
	}

	void resetDefault() {
		m_settings["xorder"] = "1";
		m_settings["yorder"] = "1";
	}

	ScharrUnitSettings getSettings() const;
};

}
}

#endif /* SCHARR_HPP_ */
