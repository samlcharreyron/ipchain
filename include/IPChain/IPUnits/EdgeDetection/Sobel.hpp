#ifndef SOBEL_HPP_
#define SOBEL_HPP_

#include "IPChain/UnitTypes/ImageFilterUnit.hpp"

namespace IRIS {
namespace Vision {

class SobelUnit : public ImageFilterUnit {
	void validateSettings();

	struct SobelUnitSettings {
		int xorder, yorder;
		int ksize;
	};

	class SobelUnitError : public std::runtime_error {
	public:
		explicit SobelUnitError(const std::string& what) : std::runtime_error(what) {}
	};

public:
	SobelUnit() { 		resetDefault(); }

	SobelUnit(const UnitSettings & settings) :
		ImageFilterUnit(settings) {
		validateSettings();
	}

	void filterImage(const cv::Mat in, cv::Mat & out) const;

	static const std::string nameS() {
		return "SobelUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Sobel operator combining smoothing in one direction and differentiation in the other.";
	}

	const std::string getName() const {
		return "Sobel Filter Unit";
	}

	void resetDefault() {
		m_settings["xorder"] = "1";
		m_settings["yorder"] = "1";
		m_settings["ksize"]  = "3";
	}

	SobelUnitSettings getSettings() const;
};
}
}

#endif /* SOBEL_HPP_ */
