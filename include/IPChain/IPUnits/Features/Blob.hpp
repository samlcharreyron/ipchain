#ifndef BLOB_HPP_
#define BLOB_HPP_

#include <stdexcept>
#include <opencv2/features2d/features2d.hpp>
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

class BlobDetectorUnit : public ExtractFeaturesKPUnit {
	void validateSettings();

	struct BlobDetectorUnitSettings {
		// OpenCV already provides a structure of parameters for
		// blob detection
		cv::SimpleBlobDetector::Params detectorParams;
		cv::Scalar color;
	};

	class BlobDetectorUnitError : public std::runtime_error {
	public:
		explicit BlobDetectorUnitError(const std::string & what) :
		std::runtime_error(what) {}
	};

public:

	BlobDetectorUnit() { 		resetDefault();}

	BlobDetectorUnit(UnitSettings & settings) :
		ExtractFeaturesUnit(settings) {
		validateSettings();
	}
    
    // static version
	static const std::string nameS() {
        return "BlobDetectorUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Extracts blobs from image.  Uses the simple blob detector.";
	}

	const std::string getName() const {
		return "Blob Detector Unit";
	}

	void resetDefault() {
		m_settings["thresholdStep"] = "10";
		m_settings["minThreshold"] = "50";
        m_settings["maxThreshold"] = "255";
		m_settings["minRepeatability"] = "2";
		m_settings["minDistBetweenBlobs"] = "30";
        m_settings["filterByColor"] = "1";
		m_settings["blobColor"] = "0";
        m_settings["filterByArea"] = "1";
		m_settings["minArea"] = "20";
		m_settings["maxArea"] = "100";
		m_settings["color"]	  = "#ff0000";
        m_settings["filterByCircularity"] = "0";
        m_settings["minCircularity"] = "0.8";
        m_settings["maxCircularity"] = "1" ;
        m_settings["filterByInertia"] = "0";
        m_settings["minInertiaRatio"] = "0.1";
        m_settings["maxInertiaRatio"] = "3.4";
        m_settings["filterByConvexity"] = "0";
        m_settings["minConvexity"] = "0.95";
        m_settings["maxConvexity"] = "3.4";
	}

protected:
    BlobDetectorUnitSettings getBlobSettings() const;

	void extractFeatures(const cv::Mat in, KeyPoints &features);

	void displayFeatures(const KeyPoints & features, const cv::Mat overlay, cv::Mat &out) const;

};


}
}



#endif /* BLOB_HPP_ */
