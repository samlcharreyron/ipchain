#ifndef HARRIS_HPP_
#define HARRIS_HPP_

#include <stdexcept>
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {
class HarrisCornerUnit : public ExtractFeaturesUnit<cv::Mat> {
	void validateSettings();

	struct HarrisCornerUnitSettings {
		int blockSize;
		int ksize;
		double k;
		double thresh;
		bool normalize;
		int drawRadius;
		cv::Scalar color;
	};

	class HarrisCornerUnitError : public std::runtime_error {
	public:
		explicit HarrisCornerUnitError(const std::string& what) : std::runtime_error(what) {}
	};

public:
	HarrisCornerUnit() {		resetDefault();}

    explicit HarrisCornerUnit(UnitSettings & settings) :
		ExtractFeaturesUnit(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "HarrisUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Detects corner features in image.";
	}

	const std::string getName() const {
		return "Harris Corner Detector";
	}

	void resetDefault() {
		m_settings["blockSize"] = "2";
		m_settings["ksize"] 	= "3";
		m_settings["k"] 		= "0.04";
		m_settings["thresh"]    = "200";
		m_settings["drawRadius"] = "5";
		m_settings["normalize"] = "1";
		m_settings["color"]     = "#FF0000";
	}

protected:
	HarrisCornerUnitSettings getSettings() const;

	void extractFeatures(const cv::Mat in, cv::Mat& features);

	void displayFeatures(cv::Mat const & features, const cv::Mat overlay, cv::Mat& out) const;
};
}
}





#endif /* HARRIS_HPP_ */
