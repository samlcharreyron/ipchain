#ifndef HOUGHCIRCLES_HPP_
#define HOUGHCIRCLES_HPP_

#include <stdexcept>
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

class HoughCirclesUnit : public ExtractFeaturesUnit<cv::Mat> {
    void validateSettings();

    struct HoughCirclesUnitSettings {
        double dp;
        double minDist;
        double thresh1;
        double thresh2;
        int    minRadius;
        int    maxRadius;
        cv::Scalar color;
        int thickness;
    };

    class HoughCirclesUnitError : public std::runtime_error {
    public:
        explicit HoughCirclesUnitError(const std::string & what) :
            std::runtime_error(what) {}
    };

public:
    HoughCirclesUnit() {		resetDefault();}

    explicit HoughCirclesUnit(UnitSettings & settings) :
        ExtractFeaturesUnit<cv::Mat>(settings) {
        validateSettings();
    }

	static const std::string nameS() {
		return "HoughCirclesUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Hough transform to detect circle shapes in the image.";
	}

    const std::string getName() const {
        return "Hough Circles Unit";
    }

    void resetDefault() {
        m_settings["dp"]            = "2";
        m_settings["minDist"]       = "20";
        m_settings["thresh1"]       = "200";
        m_settings["thresh2"]       = "100";
        m_settings["minRadius"]     = "0";
        m_settings["maxRadius"]     = "0";
        m_settings["color"]			= "#ff0000";
        m_settings["thickness"]		= "3";
    }

protected:
    HoughCirclesUnitSettings getSettings() const;

    void extractFeatures(const cv::Mat in, cv::Mat & features);

    void displayFeatures(cv::Mat const & features, const cv::Mat overlay, cv::Mat &out) const;

private:
    // Features format from OpenCV: one circle per column
    // row 0: center x, row 1: center y, row 2: radius
};

}
}


#endif /* HOUGHCIRCLES_HPP_ */
