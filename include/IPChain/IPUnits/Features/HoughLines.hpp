#ifndef HOUGHLINES_HPP_
#define HOUGHLINES_HPP_

#include <stdexcept>
#include "IPChain/UnitTypes/FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

class HoughLinesUnit : public ExtractFeaturesUnit<cv::Mat> {
    void validateSettings();

    struct HoughLinesUnitSettings {
        double rho;
        double theta;
        int threshold;
        double minLineLength;
        double maxLineGap;
        double srn;
        double stn;
        cv::Scalar color;
        int thickness;
        int mode;
    };

    class HoughLinesUnitError : public std::runtime_error {
    public:
        explicit HoughLinesUnitError(const std::string& what) :
            std::runtime_error(what) {}
    };

    enum HoughLinesMode
    {
        STANDARD,
        PROBABILISTIC,
        MULTISCALE
    };

public:
    HoughLinesUnit() { 		resetDefault();}

    explicit HoughLinesUnit(UnitSettings & settings) :
        ExtractFeaturesUnit(settings) {
        validateSettings();
    }

	static const std::string nameS() {
		return "HoughLinesUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies the Hough transform to find lines in the image.";
	}

    const std::string getName() const {
        return "Hough Lines Unit";
    }

    void resetDefault() {
        m_settings["rho"]       = "10";
        m_settings["theta"]     = "0.05";
        m_settings["threshold"] = "100";
        m_settings["minLineLength"] = "0";
        m_settings["maxLineGap"] = "0";
        m_settings["srn"]       = "0";
        m_settings["stn"]       = "0";
        m_settings["color"] 	= "#ff0000";
        m_settings["thickness"] = "1";
        m_settings["mode"]      = "0";
    }

protected:
    HoughLinesUnitSettings getSettings() const;

    void extractFeatures(const cv::Mat in, cv::Mat &features);

    void displayFeatures(const cv::Mat & features, const cv::Mat overlay, cv::Mat &out) const;

private:
    // Features format from OpenCV: one line per column
    // row 0: rho, row 1: theta

};

}
}



#endif /* HOUGHLINES_HPP_ */
