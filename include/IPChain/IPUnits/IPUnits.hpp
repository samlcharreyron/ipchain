/*
 * IProcUnits.hpp
 *
 *  Created on: Dec 3, 2013
 *      Author: Sam
 */

#ifndef IPUNITS_HPP_
#define IPUNITS_HPP_

#include <opencv2/imgproc/imgproc.hpp>
#include <IPChain/IPChain.hpp>
#include <IPChain/IPUnits/EdgeDetection/Canny.hpp>
#include <IPChain/IPUnits/EdgeDetection/Sobel.hpp>
#include <IPChain/IPUnits/EdgeDetection/Scharr.hpp>
#include <IPChain/IPUnits/EdgeDetection/Laplacian.hpp>
#include <IPChain/IPUnits/Smoothing/Smoothing.hpp>
#include <IPChain/IPUnits/Threshold/Threshold.hpp>
#include <IPChain/IPUnits/Contours/Contours.hpp>
#include <IPChain/IPUnits/Morphology/Morphology.hpp>
#include <IPChain/IPUnits/Features/Harris.hpp>
#include <IPChain/IPUnits/Features/HoughLines.hpp>
#include <IPChain/IPUnits/Features/HoughCircles.hpp>
#include <IPChain/IPUnits/Features/Blob.hpp>
#include <IPChain/IPUnits/Tracking/Kalman.hpp>
#include "IPChain/IPUnits/Rodbot/RodbotBlobDetector.hpp"
#include "IPChain/IPUnits/Rodbot/LinesRegularityFilter.hpp"
#include "IPChain/IPUnits/Rodbot/LinesAngleFilter.hpp"
#include "IPChain/IPUnits/Rodbot/RodbotKalmanFilter.hpp"

namespace IRIS {
namespace Vision {

class DummyUnit : public IPUnit
{
	void ThisProcessImage(const cv::Mat in, cv::Mat& out) const {
		out = in;
	}

	std::string ToString() const {
		return "Dummy";
	}
};

}
}
#endif /* IPUNITS_HPP_ */
