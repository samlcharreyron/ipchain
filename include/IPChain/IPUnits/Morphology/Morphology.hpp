/*
 * Morphology.hpp
 *
 *  Created on: Dec 9, 2013
 *      Author: Sam
 */

#ifndef MORPHOLOGY_HPP_
#define MORPHOLOGY_HPP_

//#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include "IPChain/UnitTypes/ImageFilterUnit.hpp"

namespace IRIS {
namespace Vision {

namespace MorphologyTypes {
enum T {
	EROSION,
	DILATION
};
}

struct MorphologySettings {
	int type;
	cv::Point anchor;
	int iterations;
	int kernelType;
	int kernelSize;
	int borderType;
	cv::Scalar borderValue;
};

class MorphologyUnit : public ImageFilterUnit {

	cv::Mat m_kernel;

public:
	MorphologyUnit() {		resetDefault(); }

	MorphologyUnit(const UnitSettings & settings) :
			ImageFilterUnit(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "MorphologyUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Erosion or dilation operations.";
	}

	const std::string getName() const {
		return "Morphology Unit";
	}

	void resetDefault() {
        m_settings["type"] = boost::lexical_cast<std::string>(MorphologyTypes::EROSION);
		m_settings["anchorX"] = "-1";
		m_settings["anchorY"] = "-1";
		m_settings["iterations"] = "1";
        m_settings["kernelType"] = boost::lexical_cast<std::string>(static_cast<int>(cv::MORPH_RECT));
        m_settings["kernelSize"] = boost::lexical_cast<std::string>(3);
	}

protected:

	MorphologySettings getSettings() const;

	void filterImage(const cv::Mat in,cv::Mat& out) const;
	void validateSettings();
	//std::string ToString() const;

};
}
}

#endif /* MORPHOLOGY_HPP_ */
