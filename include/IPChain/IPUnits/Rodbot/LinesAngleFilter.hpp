/*
 * LinesAngleFilter.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef LINESANGLEFILTER_HPP_
#define LINESANGLEFILTER_HPP_

#include <boost/lexical_cast.hpp>
#include "IPChain/UnitTypes/UnitTypes.hpp"

namespace IRIS {
namespace Vision {
class LinesAngleFilter : public CalibratedLinesFilter {
public:
	LinesAngleFilter(UnitSettings& settings) :
		CalibratedLinesFilter(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "LinesAngleFilterUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Filter lines";
	}

	const std::string getName() const {
		return "Lines Angle Filter";
	}

	void resetDefault() {

	}

protected:
	void validateSettings();

	struct CalibrationResults {
		cv::Point2f center;
	};

	void calibrate(const KeyPoints & keyPoints, LinesResults & results );

	void filterFeatures(const KeyPoints & featuresIn, const cv::Mat imageIn, KeyPoints & featuresOut, LinesResults & results);

	void displayFeatures(const KeyPoints & features, const cv::Mat overlay, cv::Mat& out) const;

	void displayResults(const LinesResults & results, const cv::Mat overlay, cv::Mat& out) const;

	CalibrationResults m_cal;

	cv::Point2f prevCenter;
	float prevOrientation;
};
}
}

#endif /* LINESANGLEFILTER_HPP_ */
