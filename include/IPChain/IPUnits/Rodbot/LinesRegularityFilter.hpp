/*
 * LinesRegularityFilter.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef LINESREGULARITYFILTER_HPP_
#define LINESREGULARITYFILTER_HPP_

#include "IPChain/UnitTypes/UnitTypes.hpp"

namespace IRIS {
namespace Vision {

class LinesRegularityFilter : public CalibratedLinesFilter {

public:
	LinesRegularityFilter() { resetDefault(); }

	LinesRegularityFilter(UnitSettings& settings) :
		CalibratedLinesFilter(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "LinesRegularityFilterUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Filter lines based on regularity for Rodbot";
	}

	const std::string getName() const {
		return "Lines Regularity Filter";
	}

	void resetDefault() {
		m_settings["nColumns"] = "12";
	}

protected:

	void validateSettings();

	struct LinesRegularityFilterSettings {
		int nColumns;
	};

	// TODO: Create a general Calibration Results map for all
	// calibrated units
	struct CalibrationResults {
		float tol2, dx, max_size2, min_size2;
	};

	LinesRegularityFilterSettings getSettings() const;

	float _score(float dists_to_line, float offset_diffs, int cols_empty);

	void calibrate(const KeyPoints & keyPoints, LinesResults & results );

	void processFeatures(const KeyPoints & featuresIn, const cv::Mat imageIn);

	void displayFeatures(const KeyPoints & features, const cv::Mat overlay, cv::Mat& out) const;

	void displayResults(const LinesResults & results, const cv::Mat overlay, cv::Mat& out) const;

	CalibrationResults m_cal;
};

}
}


#endif /* LINESREGULARITYFILTER_HPP_ */
