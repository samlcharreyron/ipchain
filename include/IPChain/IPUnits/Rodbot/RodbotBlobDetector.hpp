/*
 * Rodbot.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef RODBOT_HPP_
#define RODBOT_HPP_

#include <stdexcept>
#include "IPChain/UnitTypes/UnitTypes.hpp"

namespace IRIS {
namespace Vision {

class RodbotBlobDetector : public ExtractFeaturesKPUnit {
	void validateSettings();

	struct SimpleBlobDetectorSettings {
		int morphSize;
	};

	class SimpleBlobDetectorError : public std::runtime_error {
	public:
		explicit SimpleBlobDetectorError(const std::string& what) :
			std::runtime_error(what) {}
	};

public:

	RodbotBlobDetector() { resetDefault(); }

	RodbotBlobDetector(UnitSettings& settings) :
		ExtractFeaturesKPUnit(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "RodbotBlobDetectorUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Simple blob detector for Rodbot";
	}

	const std::string getName() const {
		return "Simple Rodbot Blob Detector";
	}

	void resetDefault() {
		m_settings["morphSize"] = "1";
	}

protected:
	SimpleBlobDetectorSettings getSettings() const;

	void extractFeatures(const cv::Mat in, KeyPoints& features);

	void displayFeatures(const KeyPoints & features, const cv::Mat overlay, cv::Mat& out) const;

};

}
}




#endif /* RODBOT_HPP_ */
