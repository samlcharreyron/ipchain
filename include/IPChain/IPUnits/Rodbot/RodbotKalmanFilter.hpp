/*
 * RodbotKalmanFilter.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef RODBOTKALMANFILTER_HPP_
#define RODBOTKALMANFILTER_HPP_

#include "IPChain/UnitTypes/UnitTypes.hpp"
#include <opencv2/video/tracking.hpp>

namespace IRIS {
namespace Vision {

class RodbotKalmanFilter : public CalibratedLinesFilter {
public:
	RodbotKalmanFilter() { resetDefault(); }
	RodbotKalmanFilter(UnitSettings& settings) :
		CalibratedLinesFilter(settings) {
		validateSettings();
	}

	static const std::string nameS() {
		return "RodbotKalmanFilterUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Extended Kalman Filter using a unicycle model";
	}

	const std::string getName() const {
		return "Rodbot Kalman Filter";
	}

	void resetDefault() {

	}

protected:

	void validateSettings();

	void calibrate(const KeyPoints & keyPoints, LinesResults & results );

	void processFeatures(const KeyPoints & featuresIn, const cv::Mat imageIn);

	void displayFeatures(const KeyPoints & features, const cv::Mat overlay, cv::Mat& out) const;

	void displayResults(const LinesResults & results, const cv::Mat overlay, cv::Mat& out) const;

	cv::KalmanFilter KF;
};

}
}



#endif /* RODBOTKALMANFILTER_HPP_ */
