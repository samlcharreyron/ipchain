#ifndef SMOOTHING_H_
#define SMOOTHING_H_

#include "IPChain/UnitTypes/ImageFilterUnit.hpp"
#include <boost/lexical_cast.hpp>

namespace IRIS {
namespace Vision {

namespace SmoothingTypes {
enum T {
    BOX,
    GAUSSIAN,
    MEDIAN
};
}

struct SmoothingSettings {
    SmoothingTypes::T type;
    int size;
    double sigmaX;
    double sigmaY;
};

class BlurUnit : public ImageFilterUnit {

	SmoothingTypes::T getSmoothingType() const;
	int getSize() const;
	double getSigmaX() const;
	double getSigmaY() const;
public:
	BlurUnit() {		resetDefault();}

	BlurUnit(const UnitSettings &settings);

	// interface functions
	void filterImage(const cv::Mat in,cv::Mat& out) const;

	void validateSettings();

	static const std::string nameS() {
		return "BlurUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies a smoothing kernel using either a box filter, Gaussian filter, or median filter.";
	}

	void resetDefault() {
        m_settings["type"] = boost::lexical_cast<std::string>(SmoothingTypes::BOX);
		m_settings["size"] = "3";
		m_settings["sigmaX"] = "1.0";
		m_settings["sigmaY"] = "1.0";
	}

	const std::string getName() const {
		return "Smoothing Unit";
	}
};
}
}

#endif /* SNOOTHING_HPP_ */
