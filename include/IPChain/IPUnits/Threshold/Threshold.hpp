/*
 * Threshold.hpp
 *
 *  Created on: Dec 3, 2013
 *      Author: Sam
 */

#ifndef THRESHOLD_HPP_
#define THRESHOLD_HPP_

#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "IPChain/UnitTypes/ImageFilterUnit.hpp"

namespace IRIS {
namespace Vision {

namespace ThresholdTypes {
enum T {
    BINARY,
    BINARY_INV,
    TRUNC,
    TO_ZERO,
    TO_ZERO_INV
};
}

namespace AdaptiveThresholdMethods {
enum T {
    MEAN,
    GAUSSIAN
};
}

struct ThresholdSettings {
    ThresholdTypes::T type;
    double value;
    double maxValue;
};

class ThresholdUnit : public ImageFilterUnit {

	// Todo: Change to ThresholdSettings struct
	int getType() const {
		return boost::lexical_cast<int>(m_settings.find("type")->second);
	}

	double getValue() const {
		return boost::lexical_cast<double>(m_settings.find("value")->second);
	}

	double getMaxValue() const {
		return boost::lexical_cast<double>(m_settings.find("maxValue")->second);
	}

public:
	ThresholdUnit() { 		resetDefault();}

	ThresholdUnit(const UnitSettings& settings) :
		ImageFilterUnit(settings) {
		validateSettings();
	}

	void filterImage(const cv::Mat in,cv::Mat& out) const;
	void validateSettings();

	std::string ToString() const;

	static const std::string nameS() {
		return "ThresholdUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Separates regions of an image based on the variation of intensity between object pixels and background pixels.";
	}

	const std::string getName() const {
		return "Threshold Unit";
	}

	void resetDefault() {
		m_settings["value"] = "127.0";
		m_settings["maxValue"] = "255.0";
        m_settings["type"] = boost::lexical_cast<std::string>(static_cast<int>(CV_THRESH_BINARY_INV));
	}
};

class AdaptiveThresholdUnit : public ImageFilterUnit {
	double getMaxValue() const {
		return boost::lexical_cast<double>(m_settings.find("maxValue")->second);
	}

	int getMethod() const {
		return boost::lexical_cast<int>(m_settings.find("method")->second);
	}

	int getType() const {
		return boost::lexical_cast<int>(m_settings.find("type")->second);
	}

	int getBlockSize() const {
		return boost::lexical_cast<int>(m_settings.find("blockSize")->second);
	}

	double getC() const {
		return boost::lexical_cast<double>(m_settings.find("C")->second);
	}

public:
	AdaptiveThresholdUnit() { resetDefault(); }

	AdaptiveThresholdUnit(const UnitSettings & settings) :
		ImageFilterUnit(settings) {
		validateSettings();
	}

	void filterImage(const cv::Mat in,cv::Mat& out) const;
	void validateSettings();

	std::string ToString() const;

	static const std::string nameS() {
		return "AdaptiveThresholdUnit";
	}
    
    const std::string name() const {
        return nameS();
    }

	static const std::string description() {
		return "Applies local thresholding in the image to account for inhomogeneous lighting conditions";
	}

	const std::string getName() const {
		return "Adaptive Threshold Unit";
	}

	void resetDefault() {
		m_settings["maxValue"] = "255.0";
        m_settings["method"] = boost::lexical_cast<std::string>(static_cast<int>(CV_ADAPTIVE_THRESH_MEAN_C));
        m_settings["type"] = boost::lexical_cast<std::string>(static_cast<int>(CV_THRESH_BINARY));
		m_settings["blockSize"] = "25";
		m_settings["C"] = "5.0";
	}
};
}
}

#endif /* THRESHOLD_HPP_ */
