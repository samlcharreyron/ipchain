/*
 * Tracking.hpp
 *
 *  Created on: Dec 10, 2013
 *      Author: Sam
 */

#ifndef KALMAN_HPP_
#define KALMAN_HPP_

#include "IPChain/Utils/Math.hpp"
#include "IPChain/Utils/Drawing.hpp"
#include "IPChain/UnitTypes/FeaturesUnit.hpp"
#include <opencv2/video/tracking.hpp>
#include <boost/lexical_cast.hpp>

namespace IRIS {
namespace Vision {

template <typename T>
class KalmanTrackerUnit : public ProcessFeaturesUnit<T> {

protected:
    struct KalmanTrackerUnitSettings {
        int dynamParams;
        int measureParams;
        int controlParams;

        float processNoiseCov;
        float measurementNoiseCov;
        float errorCovPos;

        cv::Mat transitionMatrix;
        cv::Mat measurementMatrix;

        int featureIndex; // in case of vector of features, which index to track
        double drawSize;
        cv::Scalar color;
    };

public:
    KalmanTrackerUnit() { resetDefault(); }

    KalmanTrackerUnit(UnitSettings settings) :
        ProcessFeaturesUnit<cv::Mat>(settings) {
        initialize();
        validateSettings();
    }

    void resetDefault() {
        this->m_settings["dynamParams"] = "4";
        this->m_settings["measureParams"] = "2";
        this->m_settings["controlParams"] = "0";
        this->m_settings["processNoiseCov"] = "1e-4";
        this->m_settings["measurementNoiseCov"] = "1e-1";
        this->m_settings["errorCovPos"] = "0.1";
        this->m_settings["transitionMatrix"] = "1,0,1,0,0,1,0,1,0,0,1,0,0,0,0,1";
        this->m_settings["measurementMatrix"] = "1,0,0,0,0,1,0,0";
        this->m_settings["featureIndex"] = "0";
        this->m_settings["drawSize"] = "5";
        this->m_settings["color"] = "#ff00ff";

        initialize();
    }

    void validateSettings();

    const KalmanTrackerUnitSettings getSettings() const;

    static const std::string nameS() {
        return "KalmanTrackerUnit";
    }

    static const std::string description() {
        return "Applies a Kalman filter for estimating the position of a tracked object subject to process and measurement noise.";
    }

    const std::string name() const {
        return nameS();
    }

protected:
    void initialize();

    virtual void predict(cv::Mat& state_pred, const cv::Mat& control=cv::Mat());
    virtual void correct(const cv::Mat& measurement, cv::Mat& state_corr);

    cv::Mat state_pred;
    cv::Mat state_corr;

private:
    cv::KalmanFilter kf;
};

class KalmanTrackerMatUnit : public KalmanTrackerUnit<cv::Mat> {
public:
    KalmanTrackerMatUnit() : KalmanTrackerUnit<cv::Mat>() { }

    KalmanTrackerMatUnit(UnitSettings settings) :
        KalmanTrackerUnit<cv::Mat>(settings) { }

protected:
    void processFeatures(const cv::Mat& pos_measured, const cv::Mat imageIn);
    void displayFeatures(const cv::Mat &features, const cv::Mat overlay, cv::Mat &out) const;
};

typedef cv::vector<cv::KeyPoint> KeyPoints;

class KalmanTrackerKPUnit : public KalmanTrackerUnit<KeyPoints> {
    void processFeatures(const KeyPoints& pos_measured_kp, const cv::Mat imageIn);
    void displayFeatures(const KeyPoints &features_kp, const cv::Mat overlay, cv::Mat &out) const;
};

class KalmanTrackerContoursUnit : public KalmanTrackerUnit<Contours> {
    void processFeatures(const Contours &featuresIn, const cv::Mat imageIn);
    void displayFeatures(const Contours &features, const cv::Mat overlay, cv::Mat &out) const;
};

}
}

#endif /* KALMAN_HPP_ */
