

#ifndef REGISTRAR_HPP_
#define REGISTRAR_HPP_

#include <map>
#include <string>
#include <iostream>
#include <stdexcept>

namespace IRIS {
namespace Vision {

template <typename Interface>
struct Registrar {

	class InvalidElement : public std::runtime_error {
	public:
		InvalidElement(const std::string & what) :
			std::runtime_error(what) {}
	};

	struct ClassDescriptor {
		virtual ~ClassDescriptor() {}

		virtual Interface* createInstance() const = 0;

		virtual const std::string name() const = 0;

		virtual const std::string description() const = 0;
	};

	template <typename T>
	struct GenericClassDescriptor : public ClassDescriptor {
		Interface* createInstance() const {
			return new T();
		}

		const std::string name() const {
			return T::nameS();
		}

		const std::string description() const {
			return T::description();
		}
	};

	typedef std::map<std::string, ClassDescriptor*> DescriptorMap;

	DescriptorMap classes;

	void reg(std::string name, ClassDescriptor* descriptor) {
		classes[name] = descriptor;
	}

	ClassDescriptor* getDescriptor(const std::string & name) const {
		typename DescriptorMap::const_iterator it = classes.find(name);

		if ( it == classes.end() ) {
			throw InvalidElement(std::string("Trying to load an unregistered unit: ") + name);
		}

		return it->second;
	}

	Interface* create(const std::string& name) const {
		return getDescriptor(name)->createInstance();
	}

	~Registrar() {
		typename DescriptorMap::iterator it = classes.begin();

		for(; it != classes.end() ; it++) {
			delete it->second;
		}
	}

	std::string getDescription(const std::string& name) const {
		return getDescriptor(name)->description();
	}

	//! Print the list of registered classes to stream
	void dump(std::ostream &stream) const
	{
		for (typename DescriptorMap::const_iterator it = classes.begin() ; it != classes.end(); ++it)
			stream << "- " << it->first << "\n";
	}

	typedef typename DescriptorMap::const_iterator const_iterator;

	//! begin for const iterator over classes descriptions
	const_iterator begin() const
	{
		return classes.begin();
	}
	//! end for const iterator over classes descriptions
	const_iterator end() const
	{
		return classes.end();
	}

	static const Registrar& get() {
		static Registrar instance;
		return instance;
	}

};

#define REG(name) name##Registrar

#define DEF_REGISTRAR(name) Registrar< name > name##Registrar;

#define ADD_TO_REGISTRAR(name, elementName, element) { \
		typedef Registrar< name >::GenericClassDescriptor< element > Desc; \
		name##Registrar.reg(# elementName, new Desc() ); \
}

} /* namespace Vision */
} /* namespace IRIS */

#endif /* REGISTRAR_HPP_ */
