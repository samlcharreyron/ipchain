#ifndef FEATURESFILTERUNIT_HPP_
#define FEATURESFILTERUNIT_HPP_

#include "IPUnit.hpp"
#include "UnitInterfaces.hpp"
#include "FeaturesUnit.hpp"

namespace IRIS {
namespace Vision {

template <typename F, typename R>
class CalibratedFeaturesFilterUnit : public ProcessFeaturesUnit<F>, public CalibratedFeaturesFilterUnitInterface<F,R> {
public:

	CalibratedFeaturesFilterUnit() : ProcessFeaturesUnit<F>(),
	displayFeaturesOn(false),
	displayResultsOn(false),
	calibrated(false) {}

	CalibratedFeaturesFilterUnit (const UnitSettings & settings, const bool active = true,
			const bool dispFeats = false, const bool dispRes = false) :
		ProcessFeaturesUnit<F>(settings,active),
		displayFeaturesOn(dispFeats),
		displayResultsOn(dispRes),
		calibrated(false) {}

	void thisProcessImage(const cv::Mat in, cv::Mat& out);


	R& getResults() const {
		return m_results;
	}

	void setOverlay(const cv::Mat overlay) {
		m_overlay = overlay;
	}

	void toggleFeaturesDisplay(const bool on) {
		displayFeaturesOn = on;
	}

	void toggleResultsDisplay(const bool on) {
		displayResultsOn = on;
	}

protected:
	virtual ~CalibratedFeaturesFilterUnit() {}

	R m_results;
	cv::Mat m_overlay;
	bool displayFeaturesOn;
	bool displayResultsOn;
	bool calibrated;
};

}
}

template<typename F, typename R>
inline void IRIS::Vision::CalibratedFeaturesFilterUnit<F, R>::thisProcessImage(
		const cv::Mat in, cv::Mat& out) {

    // do not draw anything
    if (!displayResultsOn) {
        in.copyTo(out);
    } else {

        this->displayResults(m_results,out,out);
    }

}

#endif /* FEATURESFILTERUNIT_HPP_ */
