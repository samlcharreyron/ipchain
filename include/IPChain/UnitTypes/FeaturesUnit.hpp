/*
 * FeaturesUnit.hpp
 *
 *  Created on: Feb 7, 2014
 *      Author: Sam
 */

#ifndef EXTRACTFEATURESUNIT_HPP_
#define EXTRACTFEATURESUNIT_HPP_

#include "UnitInterfaces.hpp"
#include "IPUnit.hpp"
//#include "ProcessFeaturesUnit.hpp"
#include <iostream>
#include <opencv2/features2d/features2d.hpp>

namespace IRIS {
namespace Vision {

// Features can have different forms (cv::Mat or vectors of keypoints etc)

typedef cv::vector<cv::KeyPoint> KeyPoints;
typedef cv::vector<cv::Point> Contour;
typedef cv::vector<Contour> Contours;

template <typename F> class ProcessFeaturesUnit;

// Abstract class for units which pass on features (either extracting or filtering)
template <typename F>
class FeaturesUnit : public IPUnit, FeaturesUnitInterface<F> {
public:
    FeaturesUnit() : IPUnit(), m_displayOverInput(true) {}

    FeaturesUnit (const UnitSettings & settings, const bool active = true, const bool displayOverInput = true) :
        IPUnit(settings,active), m_displayOverInput(displayOverInput) {}

    const F& getFeatures() const;

    virtual void processImage(const cv::Mat in, cv::Mat &out);

    void setNextFeaturesUnit(ProcessFeaturesUnit<F>* unit);

    virtual ~FeaturesUnit() {}

    // Sets overlay image to be painted on with features
    void setOverlay(const cv::Mat overlay) {
        m_overlay = overlay;
    }

    ProcessFeaturesUnit<F>* getNextFeaturesUnit() const {
        return m_nextFeaturesUnit.get();
    }

    // Load from YAML config
   #ifdef HAVE_YAML_CPP
   virtual void loadFromYaml(const YAML::Node& module);

//   IPUnit(const YAML::Node& module, const bool active=true);

   //virtual void saveToYaml(YAML::Node& module) const;
   #endif // HAVE_YAML_CPP

protected:
    F m_features;

private:
    boost::shared_ptr<ProcessFeaturesUnit<F> > m_nextFeaturesUnit;
    bool m_displayOverInput;
    cv::Mat m_overlay;

};

typedef FeaturesUnit<cv::Mat> FeaturesMatUnit;
typedef FeaturesUnit<KeyPoints> FeaturesKPUnit;
typedef FeaturesUnit<Contours> FeaturesContoursUnit;

template <typename T>
class ExtractFeaturesUnit : public FeaturesUnit<T>, ExtractFeaturesUnitInterface<T> {
public:
    ExtractFeaturesUnit () : FeaturesUnit<T>() {}

	ExtractFeaturesUnit (const UnitSettings & settings, const bool active = true, const bool displayOverInput = true) :
        FeaturesUnit<T>(settings, active, displayOverInput) {}

	// Implementation of IPUnitInterface, processing extracts features and displays them
	void thisProcessImage(const cv::Mat in, cv::Mat & out);

    virtual ~ExtractFeaturesUnit () {}

};

typedef ExtractFeaturesUnit<cv::Mat> ExtractFeaturesMatUnit;
typedef ExtractFeaturesUnit<KeyPoints> ExtractFeaturesKPUnit;
typedef ExtractFeaturesUnit<Contours> ExtractFeaturesContoursUnit;

template <typename T>
class ProcessFeaturesUnit : public FeaturesUnit<T>, public ProcessFeaturesUnitInterface<T> {
public:
    ProcessFeaturesUnit () : FeaturesUnit<T>() {}

    ProcessFeaturesUnit (const UnitSettings & settings, const bool active = true, const bool displayOverInput = true) :
        FeaturesUnit<T>(settings,active,displayOverInput) {}

    // Implementation of IPUnitInterface, processing extracts features and displays them
    virtual void thisProcessImage(const cv::Mat in, cv::Mat & out);

    void setFeaturesIn(const T& featuresIn);

    virtual ~ProcessFeaturesUnit () {}

protected:
    bool m_displayOverInput;
    T m_featuresIn;
    cv::Mat m_overlay;
    boost::shared_ptr<ProcessFeaturesUnit<T> > m_nextFeaturesUnit;
};

typedef ProcessFeaturesUnit<cv::Mat> ProcessFeaturesMatUnit;
typedef ProcessFeaturesUnit<KeyPoints> ProcessFeaturesKPUnit;
typedef ProcessFeaturesUnit<Contours> ProcessFeaturesContoursUnit;

}
}

template<typename F>
inline void IRIS::Vision::FeaturesUnit<F>::setNextFeaturesUnit(
        ProcessFeaturesUnit<F>* unit) {
    m_nextFeaturesUnit = boost::shared_ptr<ProcessFeaturesUnit<F> >(unit);
}

#ifdef HAVE_YAML_CPP
template <typename F>
void IRIS::Vision::FeaturesUnit<F>::loadFromYaml(const YAML::Node &module)
{
    YAML::const_iterator map_it(module.begin());
    for (; map_it != module.end(); map_it++) {
        std::string key = map_it->first.as<std::string>();
        if (! key.compare("nextFeaturesUnit") == 0 ) {
            m_settings[key] = map_it->second.as<std::string>();
        }
    }
}
#endif //HAVE_YAML_CPP

template <typename F>
inline const F& IRIS::Vision::FeaturesUnit<F>::getFeatures() const {
    return m_features;
}

template <typename F>
void IRIS::Vision::FeaturesUnit<F>::processImage(const cv::Mat in, cv::Mat &out)
{
    if (m_active) {
        this->thisProcessImage(in, out);

        if (m_displayOverInput)
            this->displayFeatures(m_features,in,out);
        else
            this->displayFeatures(m_features,m_overlay,out);

        if (m_nextFeaturesUnit)
            m_nextFeaturesUnit->setFeaturesIn(m_features);
    } else
        in.copyTo(out);
}

template <typename T>
inline void IRIS::Vision::ExtractFeaturesUnit<T>::thisProcessImage(const cv::Mat in, cv::Mat & out ) {

    // ATTENTION: this would not compile without the this keyword
    // there appears to be a problem with some compilers not knowing to use
    // FeaturesUnitInterface<T>::extractFeatures and displayFeatures
    this->extractFeatures(in,this->m_features);

}

template <typename T>
inline void IRIS::Vision::ProcessFeaturesUnit<T>::thisProcessImage(const cv::Mat in, cv::Mat & out ) {
    this->processFeatures(m_featuresIn, in);
}

template<typename T>
inline void IRIS::Vision::ProcessFeaturesUnit<T>::setFeaturesIn(
                const T& featuresIn) {
        m_featuresIn = featuresIn;
}


#endif /* EXTRACTFEATURESUNIT_HPP_ */
