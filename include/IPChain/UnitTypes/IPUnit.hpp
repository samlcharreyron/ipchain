#ifndef IPUNIT_H_
#define IPUNIT_H_

#include "UnitInterfaces.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/exception/all.hpp>

#ifdef HAVE_YAML_CPP
	#include <yaml-cpp/yaml.h>
#endif // HAVE_YAML_CPP

namespace IRIS {
namespace Vision {
typedef std::map<std::string, std::string > UnitSettings;

struct IPUnit : public IPUnitInterface {

	bool m_active;
	
	UnitSettings m_settings;
	
    IPUnit();

    IPUnit(const UnitSettings & settings, const bool active=true);
	
	 // Load from YAML config
    #ifdef HAVE_YAML_CPP
    virtual void loadFromYaml(const YAML::Node& module);

    IPUnit(const YAML::Node& module, const bool active=true);
    
    virtual void saveToYaml(YAML::Node& module) const;
    #endif // HAVE_YAML_CPP

	// ProcessImage processes (if the effect is on)
    virtual void processImage(const cv::Mat in, cv::Mat& out);

	// Activate/Deactivate effect
    void toggle(bool enable);

    bool isActive() const;

    void applySettings(const UnitSettings &settings);

    UnitSettings getSettings() const;

	// String representation of the unit (some identifier)
    virtual std::string printSettings() const;
	
	virtual const std::string toString() const;

};

std::ostream& operator<<(std::ostream& s, const IPUnit & u);
}
}

// Some helper macros
#define SETTING_TO_INT(settings,name) boost::lexical_cast<int>(settings.find(#name)->second)
#define SETTING_TO_DOUBLE(settings,name) boost::lexical_cast<double>(settings.find(#name)->second)
#define SETTING_TO_FLOAT(settings,name) boost::lexical_cast<float>(settings.find(#name)->second)
#endif
