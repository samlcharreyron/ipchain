#ifndef FILTERUNIT_HPP_
#define FILTERUNIT_HPP_

#include "IPUnit.hpp"
#include "UnitInterfaces.hpp"
#include "IPChain/Registrar.hpp"

namespace IRIS {
namespace Vision {
class ImageFilterUnit : public IPUnit, ImageFilterUnitInterface  {

public:

	ImageFilterUnit () : IPUnit() {}

	ImageFilterUnit (const UnitSettings & settings, const bool active = true) :
	IPUnit(settings,active) {}

	virtual void thisProcessImage(const cv::Mat in, cv::Mat & out);

	virtual ~ImageFilterUnit() {}
};

}
}

inline void IRIS::Vision::ImageFilterUnit::thisProcessImage(const cv::Mat in,cv::Mat & out) {
	filterImage(in,out);
}

#endif /* IMAGEFILTERUNIT_HPP_ */
