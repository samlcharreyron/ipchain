#ifndef POSITIONTRACKERUNIT_HPP
#define POSITIONTRACKERUNIT_HPP

#include "FeaturesUnit.hpp"
#include "UnitInterfaces.hpp"

namespace IRIS {
namespace Vision {

template <typename F, typename P>
class PositionTrackerUnit : public ProcessFeaturesUnit<F> {
public:
    PositionTrackerUnit() : ProcessFeaturesUnit(),
        m_displayPosition(false) {}

    virtual void thisProcessImage(const cv::Mat in, cv::Mat &out);

    P& getPositions() const;

protected:
    void processFeatures(const T &featuresIn, const cv::Mat imageIn);

private:
    bool m_displayPosition;
    P m_positions;
};

template <typename F, typename P>
void PositionTrackerUnit<F,P>::thisProcessImage(const cv::Mat in, cv::Mat &out)
{
    this->processFeatures(m_featuresIn,in);
}


}
}

#endif // POSITIONTRACKERUNIT_HPP
