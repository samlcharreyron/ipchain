/*
 * IPUnitInterface.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Samuel Charreyron
 */

#ifndef IPUNITINTERFACE_H_
#define IPUNITINTERFACE_H_

#include <algorithm>
#include <iterator>
#include <list>
#include <opencv2/core/core.hpp>

namespace IRIS {
namespace Vision {

// Defines all required functions for a IPUnit

class IPUnitInterface {

protected:

	// thisProcessImage is abstract function for the processing
	// performed by this unit
	virtual void thisProcessImage(const cv::Mat in, cv::Mat& out) = 0;

	// pure virtual function to validate unit settings
	// invalid settings are handled
	virtual void validateSettings() = 0;

public:
	// reset to default settings
	virtual void resetDefault() = 0;

	// Name of the instance to be used
	virtual const std::string name() const = 0;

	virtual ~IPUnitInterface() {}

};

class ImageFilterUnitInterface : public IPUnitInterface
{
protected:
	// thisProcessImage is abstract function for the processing
	// performed by this unit
	virtual void filterImage(const cv::Mat in, cv::Mat& out) const = 0;
};

template <typename T>
class FeaturesUnitInterface {
protected:
    // Display features in some way over image overlay
    // The idea is that we may want to show features over the original image which is why we have overlay
    virtual void displayFeatures(const T & features, const cv::Mat overlay, cv::Mat& out) const = 0;
};

template <typename T>
class ExtractFeaturesUnitInterface
{
protected:
	// Extract features from an input image and store them in matrix form
	virtual void extractFeatures(const cv::Mat in, T & features) = 0;
};

template <typename T>
class ProcessFeaturesUnitInterface : public FeaturesUnitInterface<T>
{
protected:
	
	// Process features, eg do some filtering
	virtual void processFeatures(const T & featuresIn, const cv::Mat imageIn) = 0;
};


template <typename F, typename R>
struct CalibratedFeaturesFilterUnitInterface {
	virtual ~CalibratedFeaturesFilterUnitInterface() {}

	// calibrate the filter based on certain features and store in results object
	virtual void calibrate(const F & features, R & results ) = 0;

	// apply a filter to the keypoints and input image and store results in object
	//virtual void filterFeatures(const F & featuresIn, const cv::Mat imageIn, F & featuresOut, R & results) = 0;

	// draw results to out using overlay base image
	virtual void displayResults(const R & results, const cv::Mat overlay, cv::Mat& out) const = 0;
};

template <typename F, typename P>
struct PositionTrackerUnitInterface {

    // extract the position of the tracked object
    virtual void track(const F& features, P& positions) = 0;

    // display the tracked objects on an overlay image
    virtual void displayPositions(const P& positions, const cv::Mat overlay, cv::Mat& out) const = 0;
};

}
}


#endif /* IPUNITINTERFACE_H_ */
