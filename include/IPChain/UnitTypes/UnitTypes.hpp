#ifndef UNITTYPES_HPP_
#define UNITTYPES_HPP_

#include "IPUnit.hpp"
#include "ImageFilterUnit.hpp"
#include "FeaturesUnit.hpp"
#include "CalibratedFeaturesFilterUnit.hpp"

namespace IRIS {
namespace Vision {
struct LinesResults {
	// resulting line
	cv::Point2f left;
	cv::Point2f right;
	cv::Point2f center;
	cv::Point2f center_filtered;
	float score;
	bool bot_detected;

	// trajectory info
	float orientation, orientation_filtered, velocity, omega;
	bool oriented;

	// region of interest for next frame
	bool roi_defined;
	cv::Rect roi_box;
	// offset, if roi is employed
	cv::Point2f offset;

	// size of the image, for computing roi of entire img
	int im_width, im_height;

	LinesResults() : orientation(0), oriented(false), score(-1), roi_defined(false), im_width(-1), im_height(-1), roi_box(0, 0, 0, 0) {}
};

typedef std::vector<cv::KeyPoint> KeyPoints;
typedef CalibratedFeaturesFilterUnit<KeyPoints, LinesResults> CalibratedLinesFilter;
}
}



#endif /* UNITTYPES_HPP_ */
