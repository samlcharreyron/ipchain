/*
 * Drawing.hpp
 *
 *  Created on: Feb 7, 2014
 *      Author: Sam
 */

#ifndef DRAWING_HPP_
#define DRAWING_HPP_

#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>

namespace IRIS {
namespace Vision {

// Convert a hex color value to one that can be used by OpenCV
cv::Scalar hexToScalar(std::string hex);

void drawKeyPoints(cv::Mat frame, const cv::Vector<cv::KeyPoint> & keyPoints, cv::Point2f offset=cv::Point2f(), bool paint_discarded_points=false);

}
}

inline cv::Scalar IRIS::Vision::hexToScalar(std::string hex) {

	if (hex[0] == '#')
		hex.erase(0,1);
    
    char * p;
    int num = std::strtol(hex.c_str(), &p , 16);

	int r = num / 0x10000;
	int g = (num / 0x100) % 0x100;
	int b = num % 0x100;

	return cv::Scalar(b,g,r);
}

inline void IRIS::Vision::drawKeyPoints(cv::Mat frame, const cv::Vector<cv::KeyPoint>& keyPoints, cv::Point2f offset, bool paint_discarded_points) {
	for (int i=0; i< keyPoints.size(); i++) {
		// green if they belong to detected points
		if (keyPoints[i].class_id != -10)
			cv::circle(frame, keyPoints[i].pt+offset, 5, cv::Scalar(0, 255, 0));
		// red otherwise
		else if (paint_discarded_points && keyPoints[i].class_id == -10)
			cv::circle(frame, keyPoints[i].pt+offset, 5, cv::Scalar(0, 0, 255));
	}
}

#endif /* DRAWING_HPP_ */
