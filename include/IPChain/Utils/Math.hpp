/*
 * Utils.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef MATH_HPP_
#define MATH_HPP_

#include <opencv2/core/core.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace IRIS {
namespace Vision {
#define NORM2(a) a.x * a.x + a.y * a.y
#define NORM(a) std::sqrt(NORM2(a))

#define DIST2(a, b) (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)
#define DIST(a, b) std::sqrt(DIST2(a,b))

#define DOT(a, b) a.x * b.x + a.y * b.y

#define NINT(a) int( a + 0.5 )

// Utility function to convert a small matrix in string representation
// ie a 2x2 identity matrix from 1,0,0,1
// the order is row by row, column by column
// returns empty cv::Mat if the string is badly formatted
cv::Mat toSquareCvMat(const std::string& matStr, const int type = CV_32F);

cv::Mat toNonSquareCvMat(const std::string& str, const int rows, const int type = CV_32F);

}
}

inline cv::Mat IRIS::Vision::toSquareCvMat(const std::string &str, const int type) {

    std::string matStr = str;
    boost::algorithm::trim(matStr);

    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, matStr, boost::algorithm::is_any_of(",;"),boost::algorithm::token_compress_on);

    // check that it is a square matrix
    int N = tokens.size();
    bool isSquare = std::sqrt(N) == floor(std::sqrt(N));

    if (!isSquare)
        throw std::runtime_error("Error converting to square matrix");
    int dim = std::sqrt(N);

    cv::Mat out(dim, dim, type);

    int n = 0;
    for (cv::MatIterator_<float> it = out.begin<float>();
         it != out.end<float>(); it++) {
        *it = boost::lexical_cast<double>(tokens[n]);
        n++;
    }

    return out;

}

inline cv::Mat IRIS::Vision::toNonSquareCvMat(const std::string &str, const int rows, const int type) {
    std::string matStr = str;
    boost::algorithm::trim(matStr);

    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, matStr, boost::algorithm::is_any_of(",;"),boost::algorithm::token_compress_on);

    // check that rows*cols = N
    int N = tokens.size();
    bool isValid = N/rows == floor(N/rows);

    if (!isValid)
        throw std::runtime_error("Error converting to non-square matrix");
    int cols = N/rows;

    cv::Mat out(rows, cols, type);

    int n = 0;
    for (cv::MatIterator_<float> it = out.begin<float>();
         it != out.end<float>(); it++) {
        *it = boost::lexical_cast<double>(tokens[n]);
        n++;
    }

    return out;
}


#endif /* MATH_HPP_ */
