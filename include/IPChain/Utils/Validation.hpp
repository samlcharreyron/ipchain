/*
 * Validation.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */

#ifndef VALIDATION_HPP_
#define VALIDATION_HPP_


namespace IRIS {
namespace Vision {

	// morphology
	#define DEFAULT_KERNEL_SIZE 3
	#define MAX_KERNEL_SIZE 50
	void valKernelSize(int& kSize) {
		if (kSize < 1 || kSize > MAX_KERNEL_SIZE) {
			kSize = DEFAULT_KERNEL_SIZE;
		}
	}
}
}



#endif /* VALIDATION_HPP_ */
