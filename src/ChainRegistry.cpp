/*
 * ChainRegistry.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: Sam
 */

#include <IPChain/ChainRegistry.hpp>
#include <IPChain/IPUnits/IPUnits.hpp>

namespace IRIS {
namespace Vision {

ChainRegistry::ChainRegistry() {

	// Filters
	ADD_TO_REGISTRAR(IPUnit,BlurUnit,BlurUnit)
	ADD_TO_REGISTRAR(IPUnit,SobelUnit,SobelUnit)
	ADD_TO_REGISTRAR(IPUnit,ScharrUnit,ScharrUnit)
	ADD_TO_REGISTRAR(IPUnit,LaplacianUnit,LaplacianUnit)
	ADD_TO_REGISTRAR(IPUnit,MorphologyUnit,MorphologyUnit)
	ADD_TO_REGISTRAR(IPUnit,ThresholdUnit,ThresholdUnit)
	ADD_TO_REGISTRAR(IPUnit,AdaptiveThresholdUnit,AdaptiveThresholdUnit)
	// Features
	ADD_TO_REGISTRAR(IPUnit,CannyUnit,CannyUnit)
	ADD_TO_REGISTRAR(IPUnit,ContoursUnit,ContoursUnit)
	ADD_TO_REGISTRAR(IPUnit,BlobDetectorUnit,BlobDetectorUnit)
	ADD_TO_REGISTRAR(IPUnit,HarrisCornerUnit,HarrisCornerUnit)
	ADD_TO_REGISTRAR(IPUnit,HoughCirclesUnit,HoughCirclesUnit)
	ADD_TO_REGISTRAR(IPUnit,HoughLinesUnit,HoughLinesUnit)
    ADD_TO_REGISTRAR(IPUnit,KalmanTrackerUnit,KalmanTrackerKPUnit)
    ADD_TO_REGISTRAR(IPUnit,KalmanTrackerCUnit,KalmanTrackerContoursUnit)
}

ChainRegistry::~ChainRegistry() {}

} /* namespace Vision */
} /* namespace IRIS */
