//
//  IPChain.cpp
//  IP Chain XCode
//
//  Created by Samuel Charreyron on 05/02/14.
//  Copyright (c) 2014 ee.ethz.ch. All rights reserved.
//

#include "IPChain/IPChain.hpp"
#include "IPChain/ChainRegistry.hpp"
#include <fstream>

IRIS::Vision::IPChain::~IPChain() {
//    iterator it = m_unit_list.begin();
//    while(it != m_unit_list.end()) {
//        Remove(it);
//    }
}

void IRIS::Vision::IPChain::ProcessImage(const cv::Mat& in, cv::Mat& out) {
	if (this->size() == 0) {
		in.copyTo(out);
	} else {
		cv::Mat in_;
		in.copyTo(in_);
		cv::Mat out_ 	= cv::Mat::zeros(in.size(),in.type());

        m_mutex.lock();

		for (const_iterator it = m_unit_list.begin();
		it != m_unit_list.end(); it++) {
            try {
			(*it)->processImage(in_, out_);
            } catch (cv::Exception& e) {
                m_mutex.unlock();
                throw IRIS::Vision::UnitError(&**it, e.msg );
            }

			out_.copyTo(in_);
		}

        m_mutex.unlock();

		out = out_;
	}
}

void IRIS::Vision::IPChain::Prepend(IPUnit* p_unit){
    m_mutex.lock();
	m_unit_list.push_front(boost::shared_ptr<IPUnit>(p_unit));
    m_mutex.unlock();
}

void IRIS::Vision::IPChain::Append(IPUnit* p_unit)
{
    m_mutex.lock();
	m_unit_list.push_back(boost::shared_ptr<IPUnit>(p_unit));
    m_mutex.unlock();
}
		   
void IRIS::Vision::IPChain::Insert(iterator position, IPUnit* p_unit)
{
    m_mutex.lock();
	m_unit_list.insert(position,boost::shared_ptr<IPUnit>(p_unit));
    m_mutex.unlock();
}

void IRIS::Vision::IPChain::Insert(const int position, IPUnit * p_unit)
{
    m_mutex.lock();

	iterator it = begin();
	for (int i = 0; i < position; i++) {
		it++;
	}

	Insert(it,p_unit);
    m_mutex.unlock();
}
		   
void IRIS::Vision::IPChain::Remove(iterator position)
{
    m_mutex.lock();
    position = m_unit_list.erase(position);
    m_mutex.unlock();
}

void IRIS::Vision::IPChain::Move(const int position, IPUnit * p_unit) {

    m_mutex.lock();

    iterator it = m_unit_list.begin();
    while (it->get() != p_unit) {
        it++;
    }
    boost::shared_ptr<IPUnit> s_p_unit(*it);
    
    iterator pos_it;
    
    if (position == 0)
        pos_it = begin();
    else if (position >= m_unit_list.size())
        pos_it = end();
    else {
        pos_it = begin();
        std::advance(pos_it, position+1);
    }
    
    m_unit_list.insert(pos_it,s_p_unit);
    m_unit_list.erase(it);
    m_mutex.unlock();
    
}

IRIS::Vision::IPChain::iterator IRIS::Vision::IPChain::Remove(IPUnit* p_unit){
    iterator it = getIterator(p_unit);
    if (it == m_unit_list.end())
        return it;
    else
    {
        m_mutex.lock();
        it = m_unit_list.erase(it);
        m_mutex.unlock();
        return it;
    }
}

int IRIS::Vision::IPChain::Index(IRIS::Vision::IPUnit *p_unit)
{
    iterator it = getIterator(p_unit);
    if (it == m_unit_list.end())
        return -1;
    else
        return std::distance(m_unit_list.begin(), it);
}

IRIS::Vision::IPChain::iterator IRIS::Vision::IPChain::getIterator(IRIS::Vision::IPUnit *p_unit) {
    m_mutex.lock();
    iterator it = m_unit_list.begin();
    for (; it != m_unit_list.end(); it++) {
        if (it->get() == p_unit) {
            m_mutex.unlock();
            return it;
        }
    }

    m_mutex.unlock();
    return m_unit_list.end();
}

IRIS::Vision::IPUnit& IRIS::Vision::IPChain::operator[](const int pos) {
    m_mutex.lock();
    iterator it = m_unit_list.begin();
    std::advance(it, pos);
    m_mutex.unlock();
    return **it;
}

IRIS::Vision::IPChain::iterator IRIS::Vision::IPChain::at(const int pos)
{
    // undefined position
    if (pos >= size() || pos < 0)
        return m_unit_list.end();

    m_mutex.lock();
    iterator it = m_unit_list.begin();
    std::advance(it, pos);
    m_mutex.unlock();
    return it;

}

std::ostream& IRIS::Vision::operator <<(std::ostream& s, const IPChain& c) {

	for (IRIS::Vision::IPChain::const_iterator it = c.begin(); it != c.end(); it++) {
		s << **it;
		s << "\n";
	}

	return s;
}

void IRIS::Vision::IPChain::loadFromYaml(const std::string& configFile) {
#ifdef HAVE_YAML_CPP
	YAML::Node config = YAML::LoadFile(configFile)["IPUnits"];
    
    //std::cout << config << std::endl;
    
    if (config.size() < 1 )
        throw std::runtime_error("YAML Parse Error: config file does not have any units");
    
	ChainRegistry reg;

    // map to hold the feature connections while the chain is created
    std::map<FeaturesKPUnit*,int> featuresUnitToNextPos;
    
    YAML::Node::const_iterator n_it = config.begin();

    // Load all the units from the config file
    for (; n_it != config.end(); n_it++) {
        IPUnit* unit;
        if (config.Type() == YAML::NodeType::Sequence) {
            if (n_it->Type() == YAML::NodeType::Scalar) {
                unit = reg.IPUnitRegistrar.create(n_it->as<std::string>());
                unit->loadFromYaml(*n_it);
            } else if (n_it->Type() == YAML::NodeType::Map) {
                YAML::const_iterator unit_node = n_it->begin();
                unit = reg.IPUnitRegistrar.create(unit_node->first.as<std::string>());
                unit->loadFromYaml(n_it->second);
                FeaturesKPUnit* featuresUnit = dynamic_cast<FeaturesKPUnit*>(unit);
                if (featuresUnit) {
                    YAML::const_iterator map_it(n_it->second.begin());
                    for (; map_it != n_it->second.end(); map_it++) {
                        std::string key = map_it->first.as<std::string>();
                        if (key == "nextFeaturesUnit")
                            featuresUnitToNextPos[featuresUnit] = map_it->second.as<int>();
                    }
                }
            } else
                throw std::runtime_error("YAML Parse ERROR: node is neither a scalar or a map");
        } else if (config.Type() == YAML::NodeType::Map) {
            unit = reg.IPUnitRegistrar.create(n_it->first.as<std::string>());
            unit->loadFromYaml(n_it->second);
            FeaturesKPUnit* featuresUnit = dynamic_cast<FeaturesKPUnit*>(unit);
            if (featuresUnit) {
                YAML::const_iterator map_it(n_it->second.begin());
                for (; map_it != n_it->second.end(); map_it++) {
                    std::string key = map_it->first.as<std::string>();
                    if (key == "nextFeaturesUnit")
                        featuresUnitToNextPos[featuresUnit] = map_it->second.as<int>();
                }
            }
        } else
            throw std::runtime_error("YAML Parse ERROR: node is neither a scalar or a map");
        
        this->Append(unit);
    }
    
    // Now handle the features connections
    for (std::map<FeaturesKPUnit*,int>::iterator it = featuresUnitToNextPos.begin();
         it != featuresUnitToNextPos.end(); it++) {
        iterator nextIt = at(it->second);
        bool nextFeaturesUnitValid = false;
        if (nextIt != end()) {
            ProcessFeaturesKPUnit* nextProcessFeaturesUnit = dynamic_cast<ProcessFeaturesKPUnit*>(nextIt->get());
            if (nextProcessFeaturesUnit) {
                it->first->setNextFeaturesUnit(nextProcessFeaturesUnit);
                nextFeaturesUnitValid = true;
            }
        }

        if (!nextFeaturesUnitValid)
        {
            std::cerr << "Invalid features connection at unit " + it->first->name() << std::endl; // TODO: log warning
        }

    }
#else //HAVE_YAML_CPP
    throw std::runtime_error("Do not have yaml-cpp");
#endif //HAVE_YAML_CPP
}


void IRIS::Vision::IPChain::saveToYaml(const std::string& configFile) {
#ifdef HAVE_YAML_CPP
	YAML::Node config;

    const_iterator it = begin();
    for (; it != end(); it++) {
        (*it)->saveToYaml(config);
        FeaturesKPUnit* featuresUnit = dynamic_cast<FeaturesKPUnit*>(&**it);
        if (featuresUnit) {
            IPUnit* nextUnit = static_cast<IPUnit*>(featuresUnit->getNextFeaturesUnit());
            if (nextUnit)
                config["IPUnits"][featuresUnit->name()]["nextFeaturesUnit"] = Index(nextUnit);
        }

    }
    
    std::cout << config << std::endl;
    
    std::ofstream fout(configFile.c_str());
    fout << config;
#else //HAVE_YAML_CPP
	throw std::runtime_error("Do not have yaml-cpp");
#endif //HAVE_YAML_CPP
}
