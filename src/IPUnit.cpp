#include "IPChain/UnitTypes/IPUnit.hpp"

IRIS::Vision::IPUnit::IPUnit() :
    m_active(true) {
}

IRIS::Vision::IPUnit::IPUnit(const UnitSettings & settings, const bool active) :
    m_active(active), m_settings(settings) {
}

#ifdef HAVE_YAML_CPP
IRIS::Vision::IPUnit::IPUnit(const YAML::Node& module, const bool active) :
    m_active(active) {
    loadFromYaml(module);
}
#endif // HAVE_YAML_CPP

void IRIS::Vision::IPUnit::processImage(const cv::Mat in, cv::Mat& out) {
if (m_active) {
    thisProcessImage(in,out);
} else
    in.copyTo(out);
}

std::ostream& IRIS::Vision::operator <<(std::ostream& s,
    const IPUnit& u) {
s << "---------------------------------" << std::endl;
s << u.name() << std::endl;
s << u.printSettings() << std::endl;
s << "unit ";
if(u.isActive())
    s << "active";
else
    s<< "inactive";
return s;
}

const std::string IRIS::Vision::IPUnit::toString() const {
    std::string out = "---------------------------------\n";
    out += name() + "\n";
    out += printSettings() + "\n";
    out += "unit ";
    if(isActive())
        out += "active";
    else
        out += "inactive";
    return out;
}

#ifdef HAVE_YAML_CPP
void IRIS::Vision::IPUnit::loadFromYaml(const YAML::Node& module){

    YAML::const_iterator map_it(module.begin());
    for (; map_it != module.end(); map_it++) {
        m_settings[map_it->first.as<std::string>()] = map_it->second.as<std::string>();
    }
}

void IRIS::Vision::IPUnit::saveToYaml(YAML::Node& module) const {

    UnitSettings::const_iterator it = m_settings.begin();

    for (; it != m_settings.end(); it++) {
        module["IPUnits"][name()][it->first] = it->second;
    }

}
#endif // HAVE_YAML_CPP

void IRIS::Vision::IPUnit::toggle(bool enable) {
    m_active = enable;
}

bool IRIS::Vision::IPUnit::isActive() const {
    return m_active;
}

void IRIS::Vision::IPUnit::applySettings(const UnitSettings &settings) {
    for(UnitSettings::const_iterator it = settings.begin(); it != settings.end(); it++)
    {
        m_settings[it->first] = it->second;
    }
    validateSettings();
}

IRIS::Vision::UnitSettings IRIS::Vision::IPUnit::getSettings() const {
    return m_settings;
}

std::string IRIS::Vision::IPUnit::printSettings() const {
    std::string output;

    for(std::map<std::string, std::string>::const_iterator it = m_settings.begin();
            it != m_settings.end(); it++) {
        output += "\n" + it->first + "\t" + it->second;
    }

    return output;
}

