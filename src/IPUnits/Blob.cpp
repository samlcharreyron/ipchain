#include <IPChain/IPUnits/Features/Blob.hpp>
#include <IPChain/Utils/Drawing.hpp>
#include <boost/lexical_cast.hpp>

void IRIS::Vision::BlobDetectorUnit::validateSettings() {
}

IRIS::Vision::BlobDetectorUnit::BlobDetectorUnitSettings IRIS::Vision::BlobDetectorUnit::getBlobSettings() const {
	cv::SimpleBlobDetector::Params params;
	params.thresholdStep =  boost::lexical_cast<float>(m_settings.find("thresholdStep")->second);
	params.minThreshold = boost::lexical_cast<float>(m_settings.find("minThreshold")->second);
	params.maxThreshold = boost::lexical_cast<float>(m_settings.find("maxThreshold")->second);
	params.minRepeatability = boost::lexical_cast<int>(m_settings.find("minRepeatability")->second);
	params.minDistBetweenBlobs = boost::lexical_cast<float>(m_settings.find("minDistBetweenBlobs")->second);
	params.filterByColor = boost::lexical_cast<bool>(m_settings.find("filterByColor")->second);
    params.blobColor = boost::lexical_cast<int>(m_settings.find("blobColor")->second);
	params.filterByArea = boost::lexical_cast<bool>(m_settings.find("filterByArea")->second);
	params.minArea = boost::lexical_cast<float>(m_settings.find("minArea")->second);
	params.maxArea = boost::lexical_cast<float>(m_settings.find("maxArea")->second);
    params.filterByCircularity = boost::lexical_cast<bool>(m_settings.find("filterByCircularity")->second);
    params.filterByConvexity = boost::lexical_cast<bool>(m_settings.find("filterByConvexity")->second);
    params.filterByInertia = boost::lexical_cast<bool>(m_settings.find("filterByInertia")->second);

	BlobDetectorUnitSettings settings;
	settings.detectorParams = params;
	settings.color = hexToScalar(m_settings.find("color")->second);
	return settings;

}

void IRIS::Vision::BlobDetectorUnit::extractFeatures(const cv::Mat in,
		KeyPoints& features) {
    BlobDetectorUnitSettings settings = getBlobSettings();
	cv::SimpleBlobDetector detector(settings.detectorParams);

	detector.detect(in,features);
}

void IRIS::Vision::BlobDetectorUnit::displayFeatures(const KeyPoints & features,
		const cv::Mat overlay, cv::Mat& out) const {

    BlobDetectorUnitSettings settings = getBlobSettings();

	cv::drawKeypoints(overlay,features,out,settings.color);

}
