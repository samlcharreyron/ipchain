#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <IPChain/IPUnits/EdgeDetection/Canny.hpp>

void IRIS::Vision::CannyUnit::extractFeatures(const cv::Mat in,cv::Mat& features) {
	cv::Canny(in,features,getThresh1(),getThresh2(),getApertureSize(),getL2Gradient());
}

inline void IRIS::Vision::CannyUnit::validateSettings() {
	//TODO: Cannny validation
}

inline double IRIS::Vision::CannyUnit::getThresh1() const {
	return boost::lexical_cast<double>(m_settings.find("thresh1")->second);
}

inline double IRIS::Vision::CannyUnit::getThresh2() const {
	return boost::lexical_cast<double>(m_settings.find("thresh2")->second);
}

inline int IRIS::Vision::CannyUnit::getApertureSize() const {
	return boost::lexical_cast<int>(m_settings.find("apertureSize")->second);
}

inline bool IRIS::Vision::CannyUnit::getL2Gradient() const {
	if (m_settings.find("l2Gradient")->second.compare("true") == 0)
		return true;
	else
		return false;
}

void IRIS::Vision::CannyUnit::displayFeatures(const cv::Mat & features,
		const cv::Mat overlay, cv::Mat& out) const {
	out.create( overlay.size(), overlay.type() );
    out = cv::Scalar(0);
	overlay.copyTo(out,features);
}
