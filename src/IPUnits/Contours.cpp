#include <queue>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/lexical_cast.hpp>
#include <IPChain/IPUnits/Contours/Contours.hpp>

void IRIS::Vision::ContoursUnit::extractFeatures(const cv::Mat in, Contours &features) {

	ContoursSettings settings = getSettings();
	
	cv::Mat cin;

    //if (in.channels() == 1)
        in.copyTo(cin);
    //else
    //    cv::cvtColor(in,cin,CV_RGB2GRAY);

    ContourFeatures contourFeatures;

    cv::findContours( cin, contourFeatures.contours, contourFeatures.hierarchy, settings.cont_mode,
			settings.cont_method, settings.cont_offset );

    features.clear();

    for( int i = 0; i< contourFeatures.contours.size(); i++ )
    {
        double area = cv::contourArea(contourFeatures.contours[i]);

        if (area > settings.area_thresh) {
            Contour contour;
            cv::approxPolyDP(contourFeatures.contours[i],contour,settings.eps_area_sf*area,true);
            short num_sides = contour.size();

            if ((num_sides == 3 && (settings.shape_options & TRIANGLES)) ||
                    (num_sides == 4 && (settings.shape_options & RECTANGLES)) ||
                    (num_sides == 6 && (settings.shape_options & LSHAPE)) ||
                    (num_sides == 8 && (settings.shape_options & EIGHTSIDES))) {
                features.push_back(contour);
            }
        }
    }

}

void IRIS::Vision::ContoursUnit::displayFeatures(const Contours & contours,
		const cv::Mat overlay, cv::Mat& out) const {

	ContoursSettings settings = getSettings();
    
    if (overlay.type() == CV_8UC1)
        cv::cvtColor(overlay, out, cv::COLOR_GRAY2RGB);
    else
        overlay.copyTo(out);


	// Now draw the polygons
    for (int i = 0; i < contours.size(); i++) {

		cv::Scalar color = cv::Scalar( 255,255,255 ); // default color: black

        short num_sides = contours[i].size();
		if (num_sides == 3) {
			color = cv::Scalar( 0,0,255 ); // red
		}

		if (num_sides == 4) {
			color = cv::Scalar( 0,255,0 ); // green
		}

		if (num_sides == 6) {
			color = cv::Scalar( 255,0,0 ); // blue
		}

		if (num_sides == 8) {
			color = cv::Scalar( 255,255,0 ); // yellow
		}

        cv::drawContours( out, contours, i, color, settings.thickness,
				settings.line_type
                , cv::noArray(), settings.max_level);
	}

}

IRIS::Vision::ContoursUnit::ContoursSettings IRIS::Vision::ContoursUnit::getSettings() const {
	ContoursSettings settings;
	try {
	settings.cont_mode = boost::lexical_cast<int>(m_settings.find("mode")->second);
	settings.cont_method = boost::lexical_cast<int>(m_settings.find("method")->second);
	settings.cont_offset = cv::Point(boost::lexical_cast<int>(m_settings.find("offsetX")->second),
			boost::lexical_cast<int>(m_settings.find("offsetY")->second));
	settings.thickness = boost::lexical_cast<int>(m_settings.find("thickness")->second);
	settings.line_type = boost::lexical_cast<int>(m_settings.find("lineType")->second);
	settings.max_level = boost::lexical_cast<int>(m_settings.find("maxLevel")->second);
	settings.shape_options = boost::lexical_cast<int>(m_settings.find("shapeOptions")->second);
	settings.area_thresh = boost::lexical_cast<double>(m_settings.find("areaThresh")->second);
	settings.eps_area_sf = boost::lexical_cast<double>(m_settings.find("epsAreaSf")->second);
	} catch (boost::bad_lexical_cast & e) {
        std::cerr << "RODBOT bad lexical cast: " << "target type: " << e.target_type().name() << std::endl;
		throw e;
	}
	if (m_settings.find("ignoreInside")->second.compare("true") == 0)
			settings.ignore_inside = true;
	else
		settings.ignore_inside = false;

	return settings;

}

inline void IRIS::Vision::ContoursUnit::validateSettings() {
    //TODO: Contours validation
}

void IRIS::Vision::ContoursCentroidUnit::processFeatures(const IRIS::Vision::Contours &featuresIn, const cv::Mat imageIn)
{
    // Get the moments
    cv::vector<cv::Moments> mu(featuresIn.size() );
    for( int i = 0; i < featuresIn.size(); i++ )
     { mu[i] = cv::moments( featuresIn[i], false ); }

    //  Get the mass centers:
    Contour mc( featuresIn.size() );
    for( int i = 0; i < featuresIn.size(); i++ )
    { mc[i] = cv::Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }

    m_features.push_back(mc);
}

void IRIS::Vision::ContoursCentroidUnit::displayFeatures(const IRIS::Vision::Contours &features, const cv::Mat overlay, cv::Mat &out) const
{
    overlay.copyTo(out);

    if (features.size() > 0) {
        for (int i = 0; i < features[0].size(); i++) {
            cv::circle(out, features[0][i], 2, cv::Scalar(255,0,0));
        }
    }
}

