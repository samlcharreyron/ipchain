#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "IPChain/IPUnits/Features/Harris.hpp"
#include "IPChain/Utils/Drawing.hpp"

void IRIS::Vision::HarrisCornerUnit::validateSettings() {
	HarrisCornerUnitSettings settings = getSettings();

	if (settings.ksize != 1 && settings.ksize != 3 && settings.ksize != 5 && settings.ksize != 7 )
		throw HarrisCornerUnitError("Invalid ksize should be 1,3,5 or 7");

}

void IRIS::Vision::HarrisCornerUnit::extractFeatures(const cv::Mat in,
		cv::Mat& features) {
	HarrisCornerUnitSettings settings = getSettings();
	cv::cornerHarris(in,features,settings.blockSize,settings.ksize,settings.k);
}

void IRIS::Vision::HarrisCornerUnit::displayFeatures(const cv::Mat & features,
		const cv::Mat overlay, cv::Mat& out) const {

	if (overlay.type()==CV_8UC1) {
		//image is grayscale
		cv::cvtColor(overlay,out,CV_GRAY2RGB);
	} else {
		overlay.copyTo(out);
	}

	HarrisCornerUnitSettings settings = getSettings();
	// Normalizing
	cv::Mat features_n;

	if (settings.normalize) {
		cv::normalize( features, features_n, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat() );
		cv::convertScaleAbs( features_n, features_n );
	} else {
		features_n = features;
	}



    // Drawing a circle around corners
	for( int i = 0; i < features_n.rows ; i++ )
	{ for( int j = 0; j < features_n.cols; j++ )
	{
		if( (int) features_n.at<float>( i,j) > settings.thresh )
		{
			cv::circle(out, cv::Point( j, i ), settings.drawRadius, settings.color, 2, 8, 0 );
		}
	}
	}
    
}

IRIS::Vision::HarrisCornerUnit::HarrisCornerUnitSettings IRIS::Vision::HarrisCornerUnit::getSettings() const {
	HarrisCornerUnitSettings settings;
	settings.blockSize 	= boost::lexical_cast<int>(m_settings.find("blockSize")->second);
	settings.ksize 		= boost::lexical_cast<int>(m_settings.find("ksize")->second);
	settings.k 			= boost::lexical_cast<double>(m_settings.find("k")->second);
	settings.thresh  	= boost::lexical_cast<double>(m_settings.find("thresh")->second);
	settings.drawRadius = boost::lexical_cast<int>(m_settings.find("drawRadius")->second);
	settings.color 		= hexToScalar(m_settings.find("color")->second);
	settings.normalize  = boost::lexical_cast<bool>(m_settings.find("normalize")->second);

	return settings;
}
