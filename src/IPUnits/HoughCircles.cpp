#include "IPChain/IPUnits/Features/HoughCircles.hpp"
#include "IPChain/Utils/Drawing.hpp"
#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void IRIS::Vision::HoughCirclesUnit::validateSettings() {
	HoughCirclesUnitSettings settings = getSettings();

	if (settings.minRadius != 0 && settings.maxRadius != 0 && settings.minRadius > settings.maxRadius) {
		throw HoughCirclesUnitError("Max radius must be larger than min radius");
	}
}

IRIS::Vision::HoughCirclesUnit::HoughCirclesUnitSettings IRIS::Vision::HoughCirclesUnit::getSettings() const {

	HoughCirclesUnitSettings settings;

	settings.dp = boost::lexical_cast<double>(m_settings.find("dp")->second);
	settings.minDist = boost::lexical_cast<double>(m_settings.find("minDist")->second);
	settings.thresh1 = boost::lexical_cast<double>(m_settings.find("thresh1")->second);
	settings.thresh2 = boost::lexical_cast<double>(m_settings.find("thresh2")->second);
	settings.minRadius = boost::lexical_cast<int>(m_settings.find("minRadius")->second);
	settings.maxRadius = boost::lexical_cast<int>(m_settings.find("maxRadius")->second);
	settings.color = hexToScalar(m_settings.find("color")->second);
	settings.thickness = boost::lexical_cast<int>(m_settings.find("thickness")->second);

	return settings;
}

void IRIS::Vision::HoughCirclesUnit::extractFeatures(const cv::Mat in,
		cv::Mat& features) {

	HoughCirclesUnitSettings settings = getSettings();
	cv::HoughCircles(in,features,CV_HOUGH_GRADIENT,settings.dp,settings.minDist,settings.thresh1,
			settings.thresh2,settings.minRadius,settings.maxRadius);
}

void IRIS::Vision::HoughCirclesUnit::displayFeatures(const cv::Mat & features,
		const cv::Mat overlay, cv::Mat& out) const {
	HoughCirclesUnitSettings settings = getSettings();

	if (out.type() == CV_8UC1) {
		cv::cvtColor(overlay, out, CV_GRAY2RGB);
	} else {
		overlay.copyTo(out);
	}

	for( int i = 0; i < features.cols; i++ )
	    {
	         cv::Point center(cvRound(features.at<float>(0,i)), cvRound(features.at<float>(1,i)));
	         int radius = cvRound(features.at<float>(2,i));
	         // draw the circle center
	         //cv::circle( out, center, 3, cv::Scalar(0,255,0), -1, 8, 0 );
	         // draw the circle outline
	         cv::circle( out, center, radius, settings.color, settings.thickness);
	    }
}
