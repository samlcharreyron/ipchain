#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "IPChain/IPUnits/Features/HoughLines.hpp"
#include "IPChain/Utils/Drawing.hpp"

void IRIS::Vision::HoughLinesUnit::validateSettings() {
    HoughLinesUnitSettings settings = getSettings();

    if (settings.rho <= 0 || settings.theta <= 0) {
    	throw HoughLinesUnitError("Invalid rho or theta resolution. Must be positive.");
    }

    if (settings.threshold < 1 || settings.threshold > 254) {
    	throw HoughLinesUnitError("Invalid threshold.  Must be between 1 and 254.");
    }
}

IRIS::Vision::HoughLinesUnit::HoughLinesUnitSettings IRIS::Vision::HoughLinesUnit::getSettings() const {
	HoughLinesUnitSettings settings;
	settings.rho = boost::lexical_cast<double>(m_settings.find("rho")->second);
	settings.theta = boost::lexical_cast<double>(m_settings.find("theta")->second);
	settings.threshold = boost::lexical_cast<int>(m_settings.find("threshold")->second);
    settings.minLineLength = SETTING_TO_DOUBLE(m_settings,minLineLength);
    settings.maxLineGap = SETTING_TO_DOUBLE(m_settings,maxLineGap);
	settings.srn = boost::lexical_cast<double>(m_settings.find("srn")->second);
	settings.stn = boost::lexical_cast<double>(m_settings.find("stn")->second);
	settings.color = hexToScalar(m_settings.find("color")->second);
	settings.thickness = boost::lexical_cast<int>(m_settings.find("thickness")->second);
    settings.mode   = SETTING_TO_INT(m_settings,mode);
	return settings;
}

void IRIS::Vision::HoughLinesUnit::extractFeatures(const cv::Mat in,
		cv::Mat& features) {
	HoughLinesUnitSettings settings = getSettings();

    switch (settings.mode)
    {
    case STANDARD:
    {
        cv::HoughLines(in,features,settings.rho,settings.theta,settings.threshold,settings.srn,settings.stn);
        break;
    }
    case PROBABILISTIC:
    {
        cv::HoughLinesP(in, features,settings.rho,settings.theta,settings.threshold, settings.minLineLength, settings.maxLineGap);
        break;
    }
    case MULTISCALE:
    {
        cv::HoughLines(in, features,settings.rho, settings.theta,settings.threshold,settings.srn,settings.stn);
        break;
    }
    }
}

void IRIS::Vision::HoughLinesUnit::displayFeatures(const cv::Mat & features,
		const cv::Mat overlay, cv::Mat& out) const {
	HoughLinesUnitSettings settings = getSettings();
    
    if (out.type() == CV_8UC1) {
        cv::cvtColor(overlay, out, CV_GRAY2RGB);
    } else {
        overlay.copyTo(out);
    }

	for (int i=0; i < features.cols; i++) {
		float rho = features.at<float>(0,i);
		float theta = features.at<float>(1,i);
		cv::Point p1, p2;
		double a = cos(theta), b= sin(theta);
		double x0 = a*rho, y0 = b*rho;
		p1.x = cvRound(x0 + 1000 * (-b));
		p1.y = cvRound(y0 + 1000 * a);
		p2.x = cvRound(x0 - 1000 * (-b));
		p2.y = cvRound(y0 - 1000 * a);

		cv::line(out, p1 , p2, settings.color, settings.thickness);
	}
}
