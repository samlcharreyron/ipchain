#include <IPChain/IPUnits/Tracking/Kalman.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define drawCross( img, center, color, d )                                 \
cv::line( img, cv::Point( center.x - d, center.y - d ),                \
cv::Point( center.x + d, center.y + d ), color, 2, CV_AA, 0); \
cv::line( img, cv::Point( center.x + d, center.y - d ),                \
cv::Point( center.x - d, center.y + d ), color, 2, CV_AA, 0 )

template <typename T>
void IRIS::Vision::KalmanTrackerUnit<T>::validateSettings() {
    KalmanTrackerUnitSettings settings = getSettings();

    assert(settings.transitionMatrix.rows == settings.dynamParams && settings.transitionMatrix.cols == settings.dynamParams);
    assert(settings.measurementMatrix.rows == settings.measureParams && settings.measurementMatrix.cols == settings.dynamParams);
}

template <typename T>
void IRIS::Vision::KalmanTrackerUnit<T>::initialize()
{
    KalmanTrackerUnitSettings settings = getSettings();
    kf.init(settings.dynamParams, settings.measureParams, settings.controlParams);
    cv::setIdentity(kf.processNoiseCov, settings.processNoiseCov);
    cv::setIdentity(kf.measurementNoiseCov, settings.measurementNoiseCov);
    cv::setIdentity(kf.errorCovPost, settings.errorCovPos);
    kf.transitionMatrix = settings.transitionMatrix;
    kf.measurementMatrix = settings.measurementMatrix;
    kf.statePre = cv::Mat::zeros(settings.dynamParams,1,CV_32F);

}

template <typename T>
const typename IRIS::Vision::KalmanTrackerUnit<T>::KalmanTrackerUnitSettings IRIS::Vision::KalmanTrackerUnit<T>::getSettings() const
{
    KalmanTrackerUnitSettings settings;
    settings.dynamParams        = SETTING_TO_INT(this->m_settings,dynamParams);
    settings.measureParams      = SETTING_TO_INT(this->m_settings,measureParams);
    settings.controlParams      = SETTING_TO_INT(this->m_settings,controlParams);
    settings.processNoiseCov    = SETTING_TO_FLOAT(this->m_settings,processNoiseCov);
    settings.measurementNoiseCov = SETTING_TO_FLOAT(this->m_settings,measurementNoiseCov);
    settings.errorCovPos        = SETTING_TO_FLOAT(this->m_settings,errorCovPos);
    settings.transitionMatrix = toSquareCvMat(this->m_settings.find("transitionMatrix")->second);
    settings.measurementMatrix = toNonSquareCvMat(this->m_settings.find("measurementMatrix")->second,settings.measureParams);
    settings.featureIndex = SETTING_TO_INT(this->m_settings,featureIndex);
    settings.drawSize = SETTING_TO_DOUBLE(this->m_settings,drawSize);
    settings.color = hexToScalar(this->m_settings.find("color")->second);
    return settings;
}

template <typename T>
void IRIS::Vision::KalmanTrackerUnit<T>::predict(cv::Mat &state_pred, const cv::Mat &control)
{
    cv::Mat ctrl = cv::Mat::ones(2,1,CV_32F);
    cv::Mat_<float> controlMatrix = cv::Mat_<float>::zeros(4,2);
    controlMatrix(0,0) = 1;
    controlMatrix(1,1) = 1;
    kf.controlMatrix = controlMatrix;
    state_pred = kf.predict(ctrl);
}

template <typename T>
void IRIS::Vision::KalmanTrackerUnit<T>::correct(const cv::Mat &measurement, cv::Mat &state_corr)
{
    state_corr = kf.correct(measurement);
    //std::cout << "K: " << kf.gain << std::endl;
}


void IRIS::Vision::KalmanTrackerMatUnit::processFeatures(const cv::Mat& pos_measured, const cv::Mat imageIn)
{
    predict(state_pred);
    correct(pos_measured, state_corr);
    m_features = state_corr;
}

void IRIS::Vision::KalmanTrackerMatUnit::displayFeatures(const cv::Mat &features, const cv::Mat overlay, cv::Mat &out) const
{
    out = overlay.clone();
    if (features.rows > 0 && features.cols > 0) {
        cv::Point point(features.at<float>(0),features.at<float>(1));
        drawCross( out, point, cv::Scalar(255,255,255), 5 );
    }
}

void IRIS::Vision::KalmanTrackerKPUnit::processFeatures(const IRIS::Vision::KeyPoints &pos_measured_kp, const cv::Mat imageIn)
{
    KalmanTrackerUnitSettings settings = getSettings();
    predict(state_pred);

    //std::cout << "predicted position : " << state_pred << std::endl;

    cv::Mat pos_measured(2,1,CV_32F);

    if (pos_measured_kp.size() > 0) {
        pos_measured.at<float>(0) = pos_measured_kp[settings.featureIndex].pt.x;
        pos_measured.at<float>(1) = pos_measured_kp[settings.featureIndex].pt.y;


        //std::cout << "measured position : " << pos_measured << std::endl;

        correct(pos_measured, state_corr);

        //std::cout << "corrected position : " << state_corr << std::endl;

        if (m_features.size() == 0) {
            m_features.push_back(pos_measured_kp[0]);
        }

        m_features[0].pt.x = state_corr.at<float>(0);
        m_features[0].pt.y = state_corr.at<float>(1);
    }
}

void IRIS::Vision::KalmanTrackerKPUnit::displayFeatures(const IRIS::Vision::KeyPoints &features_kp, const cv::Mat overlay, cv::Mat &out) const
{
    KalmanTrackerUnitSettings settings = getSettings();
    if (overlay.channels() == 1)
        cv::cvtColor(overlay,out,CV_GRAY2RGB);
    else
        out = overlay.clone();

    if (features_kp.size() > 0) {
        cv::Point point(features_kp[0].pt.x, features_kp[0].pt.y);
        drawCross( out, point, settings.color, settings.drawSize );
    }
}

void IRIS::Vision::KalmanTrackerContoursUnit::processFeatures(const IRIS::Vision::Contours &featuresIn, const cv::Mat imageIn)
{
    predict(state_pred);

    //std::cout << "predicted position : " << state_pred << std::endl;

    cv::Mat pos_measured(2,1,CV_32F);

    if (featuresIn.size() > 0) {
        // Get the moments
        cv::vector<cv::Moments> mu(featuresIn.size() );
        for( int i = 0; i < featuresIn.size(); i++ )
         { mu[i] = cv::moments( featuresIn[i], false ); }

        //  Get the mass centers:
        cv::vector<cv::Point2f> mc( featuresIn.size() );
        for( int i = 0; i < featuresIn.size(); i++ )
        { mc[i] = cv::Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }

        pos_measured.at<float>(0) = mc[0].x;
        pos_measured.at<float>(1) = mc[0].y;

        //std::cout << "measured position : " << pos_measured << std::endl;

        if (cv::checkRange(pos_measured))
            correct(pos_measured, state_corr);

        //std::cout << "corrected position : " << state_corr << std::endl;

        m_features = featuresIn;

        for (int i = 0; i < m_features[0].size(); i++) {
            m_features[0][i] = m_features[0][i] + cv::Point(state_corr.at<float>(0) - mc[0].x, state_corr.at<float>(1) - mc[0].y);
        }
    } else {
        if (m_features.size() == 0) {
            m_features.push_back(Contour());
        }

        for (int i = 0; i < m_features[0].size(); i++) {
            m_features[0][i] = cv::Point(state_pred.at<float>(0), state_pred.at<float>(1));
        }
    }
}

void IRIS::Vision::KalmanTrackerContoursUnit::displayFeatures(const Contours &features, const cv::Mat overlay, cv::Mat &out) const
{
    KalmanTrackerUnitSettings settings = getSettings();
    if (overlay.channels() == 1)
        cv::cvtColor(overlay,out,CV_GRAY2RGB);
    else
        out = overlay.clone();

    if (features[0].size() > 0) {
        // Get the moments
        cv::Moments mu = cv::moments( features[0], false );

        // if all equal, centroid is just first point
        cv::Point point;
        if (mu.m00 == 0)
            point = features[0][0];
        else {
            //  Get the mass center:
            cv::Point mc = cv::Point( mu.m10/mu.m00 , mu.m01/mu.m00 );

            point = cv::Point(mc.x, mc.y);
        }

        drawCross( out, point, settings.color, settings.drawSize );
    }
}

