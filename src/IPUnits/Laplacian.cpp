#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include <IPChain/IPUnits/EdgeDetection/Laplacian.hpp>

void IRIS::Vision::LaplacianUnit::validateSettings() {
	LaplacianUnitSettings settings = getSettings();

	if (settings.ksize < 1 || (settings.ksize % 2) == 0 )
		throw LaplacianUnitError("Invalid ksize, must be odd and positive");
}

void IRIS::Vision::LaplacianUnit::filterImage(const cv::Mat in,
		cv::Mat& out) const {
	LaplacianUnitSettings settings = getSettings();
	cv::Laplacian(in,out,-1,settings.ksize);
}

inline IRIS::Vision::LaplacianUnit::LaplacianUnitSettings IRIS::Vision::LaplacianUnit::getSettings() const {
	LaplacianUnitSettings settings;
	settings.ksize = boost::lexical_cast<int>(m_settings.find("ksize")->second);
	return settings;
}
