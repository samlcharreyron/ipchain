
#include <boost/lexical_cast.hpp>
#include <IPChain/IPUnits/Morphology/Morphology.hpp>

void IRIS::Vision::MorphologyUnit::filterImage(const cv::Mat in,
		cv::Mat& out) const {
	MorphologySettings settings = getSettings();

	if (settings.type == MorphologyTypes::EROSION)
		cv::erode(in,out,m_kernel,settings.anchor,settings.iterations,cv::BORDER_CONSTANT,cv::morphologyDefaultBorderValue());
	else
		cv::dilate(in,out,m_kernel,settings.anchor,settings.iterations,cv::BORDER_CONSTANT,cv::morphologyDefaultBorderValue());
}

inline void IRIS::Vision::MorphologyUnit::validateSettings() {
	int size = boost::lexical_cast<int>(m_settings["kernelSize"]);
	if (size % 2 == 0 || size < 1)
		size = size + 1;
	m_kernel = cv::getStructuringElement(boost::lexical_cast<int>(m_settings["kernelType"]),cv::Size(size,size));
}

IRIS::Vision::MorphologySettings IRIS::Vision::MorphologyUnit::getSettings() const {
	MorphologySettings settings;
	try {
		settings.type = boost::lexical_cast<int>(m_settings.find("type")->second);
		settings.iterations = boost::lexical_cast<int>(m_settings.find("iterations")->second);
		settings.anchor = cv::Point(boost::lexical_cast<int>(m_settings.find("anchorX")->second),
				boost::lexical_cast<int>(m_settings.find("anchorY")->second));
		settings.kernelType = boost::lexical_cast<int>(m_settings.find("kernelType")->second);
		settings.kernelSize = boost::lexical_cast<int>(m_settings.find("kernelSize")->second);

	} catch (boost::bad_lexical_cast & e) {
		std::cerr << "RODBOT bad lexical cast: " << "target type: " << e.target_type().name() << std::endl;
		throw e;
	}

	return settings;
}
