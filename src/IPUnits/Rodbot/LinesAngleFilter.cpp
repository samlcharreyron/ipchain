#include "IPChain/IPUnits/Rodbot/LinesAngleFilter.hpp"
#include "IPChain/Utils/Math.hpp"

void IRIS::Vision::LinesAngleFilter::validateSettings() {
}

void IRIS::Vision::LinesAngleFilter::calibrate(const KeyPoints& keyPoints,
		LinesResults& results) {
	calibrated = true;
	prevCenter = results.center;
}

void IRIS::Vision::LinesAngleFilter::filterFeatures(
		const KeyPoints& featuresIn, const cv::Mat imageIn,
		KeyPoints& featuresOut, LinesResults& results) {
	if (calibrated && results.bot_detected) {
		cv::Point2f dir = results.center + results.offset - prevCenter;

		// compute velocity, return if dx was too small
		results.velocity = NORM(dir);
		if (results.velocity < 3) return;

		float angle_dir = std::atan2(dir.y, dir.x);

		cv::Point2f baseline = results.left-results.right;
		results.orientation = std::atan2(baseline.y, baseline.x) + 0.5*M_PI;

		float angle_diff = angle_dir - results.orientation;
		angle_diff += (angle_diff > M_PI) ? -2*M_PI : (angle_diff < -M_PI) ? 2*M_PI : 0;

		if (std::abs(angle_diff) > .5*M_PI) results.orientation += M_PI;

		// compute angular velocity
		results.omega = results.orientation-prevOrientation;

		prevOrientation = results.orientation;
		prevCenter = results.center + results.offset;
		results.oriented = true;
	} else {
		results.oriented = false;
	}

	featuresOut = featuresIn;
}

void IRIS::Vision::LinesAngleFilter::displayFeatures(
		const KeyPoints& features, const cv::Mat overlay, cv::Mat& out) const {
	//TODO implement
	overlay.copyTo(out);
}

void IRIS::Vision::LinesAngleFilter::displayResults(
		const LinesResults& results, const cv::Mat overlay,
		cv::Mat& out) const {
	//TODO implement
	overlay.copyTo(out);
}
