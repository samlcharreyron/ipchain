#include "IPChain/IPUnits/Rodbot/LinesRegularityFilter.hpp"
#include "IPChain/Utils/Math.hpp"
#include <boost/lexical_cast.hpp>

void IRIS::Vision::LinesRegularityFilter::validateSettings() {
	if ( SETTING_TO_INT(m_settings,nColumns) < 1) {
		m_settings["nColumns"] = "12";
	}
}

IRIS::Vision::LinesRegularityFilter::LinesRegularityFilterSettings IRIS::Vision::LinesRegularityFilter::getSettings() const {
	LinesRegularityFilterSettings settings;

	settings.nColumns = SETTING_TO_INT(m_settings, nColumns);

	return settings;
}

void IRIS::Vision::LinesRegularityFilter::calibrate(
		const KeyPoints& keyPoints, LinesResults& results) {
	LinesRegularityFilterSettings settings = getSettings();

	float distances_tot = 0;
	int n_good = 0;

	for (int i=0; i<keyPoints.size(); i++) {
		if (keyPoints[i].class_id != -10) {
			distances_tot += DIST(keyPoints[i].pt, results.left);
			n_good++;
		}
	}

	m_cal.dx = distances_tot / (0.5*n_good*(n_good-1));
	calibrated = true;

	m_cal.max_size2 = (n_good+1) * m_cal.dx;
	m_cal.max_size2 *= m_cal.max_size2;
	m_cal.min_size2 = 0.3 * n_good * m_cal.dx;
	m_cal.min_size2 *= m_cal.min_size2;
	m_cal.tol2 = 0.5* m_cal.dx;
	m_cal.tol2 *= m_cal.tol2;

}

void IRIS::Vision::LinesRegularityFilter::processFeatures(
		const KeyPoints& featuresIn, const cv::Mat imageIn) {

	LinesRegularityFilterSettings settings = getSettings();

	int N = featuresIn.size();

    m_features = featuresIn;

	struct _best {
		float score;
		int count;
		int pta;
		int ptb;

		_best() : score(-1e6), count(-1), pta(-1), ptb(-1) {}
	};

	_best best;
	std::vector<bool> maskBest(N);
	std::vector<bool> maskNow(N);
	std::vector<int> cols_ref;
	std::vector<float> cols_dists;
	std::vector<float> cols_offset_diffs;

	if (calibrated) {
		cols_ref.resize(settings.nColumns);
		cols_dists.resize(settings.nColumns);
		cols_offset_diffs.resize(settings.nColumns);
	}

	int count, cols_empty;
	float t, dist2, distToLine, offset_diffs, od;
	cv::Point2f a,b,v,p;

	float dx_inv = 1./m_cal.dx;

	if (N!=0) {
		for (int pta=0; pta < N; pta++) {
			for (int ptb=pta+1; ptb<N; ptb++) {
				count = 0;
				cols_empty = settings.nColumns;

				if (calibrated) std::fill(cols_ref.begin(), cols_ref.end(), -1);
				std::fill(maskNow.begin(), maskNow.end(), false);

				// define some basic parameters
				a = featuresIn[pta].pt;
				b = featuresIn[ptb].pt;
				distToLine = 0;
				offset_diffs = 0;
				v = b-a;

				float l2 = NORM2(v);
				float l2_sqrt = std::sqrt(l2);

				if (calibrated && (l2 > m_cal.max_size2 || l2 < m_cal.min_size2))
					continue;

				// NOTE: All computations done in square, they only need to be comparable
				float l2_inv = 1./l2;
				//cv::Mat imgpaint = frame.clone();

				// compute the distance of every point to the line segment
				for (int i=0; i<N; i++) {
					// if this point is one of the edges of our line, include them anyway
					//if (i==pta || i==ptb) {
					//
					//}
					// compute offset t in direction of v
					cv::Point2f p_ = featuresIn[i].pt-a;
					t = DOT(p_, v) * l2_inv;
					// if "before" a
					if (t<0.0)
						continue;
					//dist2 = _dist2(keyPoints[i].pt, a);
					// if "after" b
					else if (t>1.0)
						continue;
					//dist2 = _dist2(keyPoints[i].pt, b);
					// if in between the two
					else {
						p = a + t*v;
						dist2 = DIST2(featuresIn[i].pt, p);
					}

					if (dist2 < m_cal.tol2) {
						//cv::circle(imgpaint, keyPoints[i].pt, 5, cv::Scalar(0, 255, 0));
						if (calibrated) {
							// get id of column this point belongs to
							int k_col = NINT(t*l2_sqrt*dx_inv);
							od = std::abs(k_col*m_cal.dx-t*l2_sqrt);
							// check if column is valid
							if (k_col < 0 || k_col >= settings.nColumns)
								continue;
							// check if this column is already belonging to another point
							if (cols_ref[k_col] != -1) {
								int other = cols_ref[k_col];
								float score_before = _score(distToLine, offset_diffs, cols_empty);
								float score_after = _score(distToLine-cols_dists[other]+dist2, offset_diffs-cols_offset_diffs[other]+od, cols_empty);
								// check if this distance is closer to line
								if (score_before < score_after) {
									// if yes, exchange point, otherwise do nothing
									distToLine -= cols_dists[k_col];
									distToLine += dist2;
									offset_diffs -= cols_offset_diffs[k_col];
									offset_diffs += od;
									maskNow[cols_ref[k_col]] = false;
									maskNow[i] = true;
									cols_dists[k_col] = dist2;
									cols_offset_diffs[k_col] = od;
									cols_ref[k_col] = i;
								}
							}
							else {
								distToLine += dist2;
								offset_diffs += od;
								cols_dists[k_col] = dist2;
								cols_offset_diffs[k_col] = od;
								cols_ref[k_col] = i;
								maskNow[i] = true;
								count++;
								cols_empty--;
							}
						} else {
							// not calibrated;
							count++;
							distToLine += dist2;
							maskNow[i] = true;
						}
					}
				}

				/*cv::line(imgpaint, a, b, cv::Scalar(255, 0, 0), 2);
	                cv::("test", imgpaint);
	                cv::waitKey();*/

				float score;
				if (calibrated)
					score = _score(distToLine, offset_diffs, cols_empty);
				else
					score = count;

				//std::cout << "Score: " << score << std::endl;

				//dists.push_back(dist);
				//if (count > best[0] || (count == best[0] && distToLine < best[3])) {
				if (score > best.score) {
					//std::cout << "new best " << std::endl;
					/*if (calibrated) {
	                        std::cout << "cols_ref: ";
	                        for (int i=0; i<nColumns; i++) {
	                            std::cout << cols_ref[i] << ", ";
	                        }
	                        std::cout << std::endl;
	                    }*/
					best.score = score;
					best.count = count;
					best.pta = pta;
					best.ptb = ptb;
					//best[3] = distToLine;
					std::swap(maskBest, maskNow);
				}
			}
		}

        m_results.left = m_features[best.pta].pt;
        m_results.right = m_features[best.ptb].pt;
		m_results.center = 0.5*(m_results.left+m_results.right);
		m_results.score = best.score;
		m_results.bot_detected = (m_results.score > .5*settings.nColumns ? true : false);

		//cv::Mat imgpaint=frame.clone();
		//cv::line(imgpaint, keyPoints[best.pta].pt, keyPoints[best.ptb].pt, cv::Scalar(0, 0, 255), 3);

		for (int i=0; i<N; i++){
			if (!maskBest[i]) {
				//cv::circle(imgpaint, keyPoints[i].pt, 5, cv::Scalar(0, 0, 255));
                m_features[i].class_id = -10;
			}
			else {
				//cv::circle(imgpaint, keyPoints[i].pt, 5, cv::Scalar(0, 255, 0));
			}
		}
		//imshow("test", imgpaint);

	}
}

void IRIS::Vision::LinesRegularityFilter::displayFeatures(
		const KeyPoints& features, const cv::Mat overlay, cv::Mat& out) const {
	//TODO: implement display
	overlay.copyTo(out);
}

float IRIS::Vision::LinesRegularityFilter::_score(float dists_to_line,
		float offset_diffs, int cols_empty) {
	LinesRegularityFilterSettings settings = getSettings();
    float dx_inv = 1./m_cal.dx;
    return (settings.nColumns-cols_empty)-(dists_to_line*dx_inv*dx_inv+offset_diffs*dx_inv*2);
}

void IRIS::Vision::LinesRegularityFilter::displayResults(
		const LinesResults& results, const cv::Mat overlay,
		cv::Mat& out) const {
	//TODO: implement display
	overlay.copyTo(out);
}
