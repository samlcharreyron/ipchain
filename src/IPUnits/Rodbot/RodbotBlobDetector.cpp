/*
 * Rodbot.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: Sam
 */
#include "IPChain/IPUnits/Rodbot/RodbotBlobDetector.hpp"
#include "IPChain/Utils/Validation.hpp"
#include "IPChain/Utils/Math.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>

void IRIS::Vision::RodbotBlobDetector::validateSettings() {
	SimpleBlobDetectorSettings settings = getSettings();
	valKernelSize(settings.morphSize);
}

IRIS::Vision::RodbotBlobDetector::SimpleBlobDetectorSettings IRIS::Vision::RodbotBlobDetector::getSettings() const {
	SimpleBlobDetectorSettings settings;
	settings.morphSize = SETTING_TO_INT(m_settings,morphSize);
	return settings;
}

void IRIS::Vision::RodbotBlobDetector::extractFeatures(const cv::Mat in,
		KeyPoints& keypoints) {

	SimpleBlobDetectorSettings settings = getSettings();

	// hardcoded params
	cv::SimpleBlobDetector::Params p;

	p.minDistBetweenBlobs = .1f;
	p.filterByInertia = false;
	p.filterByConvexity = false;
	p.filterByColor = false;
	p.filterByCircularity = false;
	p.filterByArea = true;
	p.minCircularity = .5;
	p.maxCircularity = 10;
	p.minArea = 10;
	p.maxArea = 130;
	p.minThreshold = 0;
	p.maxThreshold = 2;
	p.thresholdStep = 1;

	//cv::Mat gs, bn, bn_morph, element;
    cv::Mat processed = in.clone();

	cv::adaptiveThreshold(processed, processed, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 25, 24); // mean:9/15, gauss:25/26 | 35/31 | 51, 48 | 21,18

	std::vector<cv::KeyPoint> kp_noMorph;

	// detect features from unmodified image
	cv::FeatureDetector* blob_detector = new cv::SimpleBlobDetector(p);
	blob_detector->create("SimpleBlob");
	blob_detector->detect(processed, kp_noMorph);

	cv::Mat element;
	cv::getStructuringElement(2, cv::Size(2*settings.morphSize+1, 2*settings.morphSize+1),
			cv::Point(settings.morphSize, settings.morphSize));
	cv::morphologyEx(processed, processed, 3, element);

	blob_detector->detect(processed, keypoints);

	// compare the keypoints of morph and no morph, decide if they are the same or not
	bool found;
	for (int i=0; i<kp_noMorph.size(); i++) {
		found = false;
		for (int j=0; j<keypoints.size(); j++) {
			if (DIST(kp_noMorph[i].pt, keypoints[j].pt) < 3.) {
				found = true;
				continue;
			}
		}

		if (!found)
			keypoints.push_back(kp_noMorph[i]);
	}

}

void IRIS::Vision::RodbotBlobDetector::displayFeatures(
		const KeyPoints& features, const cv::Mat overlay, cv::Mat& out) const {
	// For now don't display anything
	overlay.copyTo(out);
}
