#include "IPChain/IPUnits/Rodbot/RodbotKalmanFilter.hpp"

void IRIS::Vision::RodbotKalmanFilter::validateSettings() {
}

void IRIS::Vision::RodbotKalmanFilter::calibrate(const KeyPoints& keyPoints,
		LinesResults& results) {
	KF.statePost.at<float>(0) = results.center.x + results.offset.x;
	KF.statePost.at<float>(1) = results.center.y + results.offset.y;
	KF.statePost.at<float>(2) = 0;
	KF.statePost.at<float>(3) = 0;
	KF.statePost.at<float>(4) = 0;

	calibrated = true;
}

void IRIS::Vision::RodbotKalmanFilter::processFeatures(
		const KeyPoints& featuresIn, const cv::Mat imageIn) {
	if (!calibrated)
		return;

	if (m_results.bot_detected) {
		float damping_omega = .5;
		float damping_velocity = 1;

		// extended kalman filter
		// transition function: f = [x+v*cos(theta+omega/2); y+v*sin(theta+omega/2); theta+omega; v; omega]
		float x = KF.statePost.at<float>(0);
		float y = KF.statePost.at<float>(1);
		float theta = KF.statePost.at<float>(2);
		float v = KF.statePost.at<float>(3)*damping_velocity;
		float omega = KF.statePost.at<float>(4);

		float ct = std::cos(theta);
		float st = std::sin(theta);
		float cto = std::cos(theta+damping_omega*omega);
		float sto = std::sin(theta+damping_omega*omega);

		KF.transitionMatrix = (cv::Mat_<float>(5,5) << 1,0,-.5*v*(st+sto),0.5*(ct+cto),-.5*v*sto, 0,1,.5*v*(ct+cto),.5*(st+sto),.5*v*cto, 0,0,1,0,damping_omega, 0,0,0,1,0, 0,0,0,0,damping_omega);

		KF.predict();

		// overwrite predict state
		KF.statePre.at<float>(0) = x + .5*v*(ct + cto);
		KF.statePre.at<float>(1) = y + .5*v*(st + sto);

		cv::Mat_<float> measured(5,1);
		measured << m_results.center.x + m_results.offset.x, m_results.center.y + m_results.offset.y,
				m_results.orientation, m_results.velocity, m_results.omega;

		cv::Mat estimated = KF.correct(measured);
		m_results.center_filtered.x = estimated.at<float>(0);
		m_results.center_filtered.y = estimated.at<float>(1);
		m_results.orientation_filtered = estimated.at<float>(2);

	}
}

void IRIS::Vision::RodbotKalmanFilter::displayFeatures(
		const KeyPoints& features, const cv::Mat overlay, cv::Mat& out) const {
	//TODO
	overlay.copyTo(out);
}

void IRIS::Vision::RodbotKalmanFilter::displayResults(
		const LinesResults& results, const cv::Mat overlay,
		cv::Mat& out) const {
	overlay.copyTo(out);
}
