#include <boost/lexical_cast.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <IPChain/IPUnits/EdgeDetection/Scharr.hpp>

void IRIS::Vision::ScharrUnit::validateSettings() {
	ScharrUnitSettings settings = getSettings();
	if (settings.xorder < 0 || settings.yorder < 0)
		throw ScharrUnitError("Order of derivatives must be positive");
}

void IRIS::Vision::ScharrUnit::filterImage(const cv::Mat in,
		cv::Mat& out) const {
	ScharrUnitSettings settings = getSettings();
	cv::Scharr(in,out,-1,settings.xorder,settings.yorder);
}

inline IRIS::Vision::ScharrUnit::ScharrUnitSettings IRIS::Vision::ScharrUnit::getSettings() const {
	ScharrUnitSettings settings;
	settings.xorder = boost::lexical_cast<int>(m_settings.find("xorder")->second);
	settings.yorder = boost::lexical_cast<int>(m_settings.find("yorder")->second);
	return settings;
}
