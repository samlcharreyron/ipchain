#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include <IPChain/IPUnits/Smoothing/Smoothing.hpp>

void IRIS::Vision::BlurUnit::filterImage(const cv::Mat in,cv::Mat& out) const {
	if (getSmoothingType() == SmoothingTypes::BOX) {
		cv::blur( in, out, cv::Size( getSize(), getSize() ));
	} else if (getSmoothingType() == SmoothingTypes::GAUSSIAN) {
		cv::GaussianBlur(in,out,cv::Size( getSize(), getSize()),
				getSigmaX(),getSigmaY());
	} else if (getSmoothingType() == SmoothingTypes::MEDIAN) {
		medianBlur(in,out,getSize());
	} else {
		in.copyTo(out);
	}
}

void IRIS::Vision::BlurUnit::validateSettings() {
	SmoothingTypes::T type = getSmoothingType();
	if (type != SmoothingTypes::BOX &&
			type!= SmoothingTypes::GAUSSIAN &&
			type != SmoothingTypes::MEDIAN ) {
        m_settings["type"] = boost::lexical_cast<std::string>(SmoothingTypes::BOX);
		std::cerr << "Warning: invalid smoothing type, automatically set to box filter" << std::endl;
	}

	if ((getSize()%2 ==0)&&(getSize() !=0)) {
        m_settings["size"] = boost::lexical_cast<std::string>(getSize() + 1);
		std::cerr << "Warning: invalid smoothing size, automatically set to " << getSize() + 1 << std::endl;
	}

	if (getSmoothingType() == SmoothingTypes::GAUSSIAN && (getSigmaX() == 0.0 || getSigmaY() == 0.0)) {
        m_settings["sigmaX"] = boost::lexical_cast<std::string>(1.0);
        m_settings["sigmaY"] = boost::lexical_cast<std::string>(1.0);
		std::cerr << "Warning: invalid sigmaX/sigmaY, automatically set to 1.0" << std::endl;
	}
}

IRIS::Vision::SmoothingTypes::T IRIS::Vision::BlurUnit::getSmoothingType() const {
	int type = boost::lexical_cast<int>(m_settings.find("type")->second);
	return static_cast<SmoothingTypes::T>(type);
}

int IRIS::Vision::BlurUnit::getSize() const {
	return boost::lexical_cast<int>(m_settings.find("size")->second);
}

double IRIS::Vision::BlurUnit::getSigmaX() const {
	return boost::lexical_cast<double>(m_settings.find("sigmaX")->second);
}

double IRIS::Vision::BlurUnit::getSigmaY() const {
	return boost::lexical_cast<double>(m_settings.find("sigmaY")->second);
}

IRIS::Vision::BlurUnit::BlurUnit(const UnitSettings& settings) : ImageFilterUnit(settings) {
	validateSettings();
}
