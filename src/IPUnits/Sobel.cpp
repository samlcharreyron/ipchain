#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include <IPChain/IPUnits/EdgeDetection/Sobel.hpp>

void IRIS::Vision::SobelUnit::validateSettings() {

	SobelUnitSettings settings = getSettings();

	if (settings.xorder < 0 || settings.yorder << 0)
		throw SobelUnitError("Invalid xorder or yorder");

	if (settings.ksize != 1 && settings.ksize != 3 && settings.ksize != 5 && settings.ksize != 7 )
		throw SobelUnitError("Invalid ksize should be 1,3,5 or 7");

	if (settings.ksize == 1 && (settings.xorder > 2 || settings.yorder > 2))
		throw SobelUnitError("If ksize = 1, xorder or yorder cannot exceed 2");

}

void IRIS::Vision::SobelUnit::filterImage(const cv::Mat in,
		cv::Mat& out) const {
	SobelUnitSettings settings = getSettings();
    cv::Mat grad;
    cv::Sobel(in,grad,CV_16S,settings.xorder,settings.yorder,settings.ksize);
    cv::convertScaleAbs( grad, out );
}

inline IRIS::Vision::SobelUnit::SobelUnitSettings IRIS::Vision::SobelUnit::getSettings() const {
	SobelUnitSettings settings;
	settings.ksize = boost::lexical_cast<int>(m_settings.find("ksize")->second);
	settings.xorder = boost::lexical_cast<int>(m_settings.find("xorder")->second);
	settings.yorder = boost::lexical_cast<int>(m_settings.find("yorder")->second);
	return settings;
}


