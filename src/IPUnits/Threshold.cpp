#include <IPChain/IPUnits/Threshold/Threshold.hpp>

void IRIS::Vision::ThresholdUnit::filterImage(const cv::Mat in,cv::Mat& out) const {
	threshold(in,out,getValue(),getMaxValue(),getType());
}

inline void IRIS::Vision::ThresholdUnit::validateSettings() {
	// TODO: Implement threshold validation
}

inline std::string IRIS::Vision::ThresholdUnit::ToString() const {
	return "Basic threshold\nthreshold value:\t"
            + boost::lexical_cast<std::string>(getValue())
    + "\nmax value:\t" + boost::lexical_cast<std::string>(getMaxValue())
    + "\ntype:\t" + boost::lexical_cast<std::string>(getType());
}

void IRIS::Vision::AdaptiveThresholdUnit::filterImage(
		const cv::Mat in,cv::Mat& out) const {
	adaptiveThreshold(in,out,getMaxValue(),getMethod(),getType(),getBlockSize(),getC());
}

inline void IRIS::Vision::AdaptiveThresholdUnit::validateSettings() {
	// TODO: Implement adaptive threshold validation
}

inline std::string IRIS::Vision::AdaptiveThresholdUnit::ToString() const {
	return "Adaptive threshold\nmax value:\t"
        + boost::lexical_cast<std::string>(getMaxValue())
        + "\nmethod:\t" + boost::lexical_cast<std::string>(getMethod())
        + "\ntype:\t" + boost::lexical_cast<std::string>(getType())
        + "\nblock size:\t" + boost::lexical_cast<std::string>(getBlockSize())
        + "\nC:\t" + boost::lexical_cast<std::string>(getC());
}
